2016-04-13 06:21:05: Algorithm Selected: KNN_WITH_PW
2016-04-13 06:21:05: GPS Dataset: 6_7_GPSPoints.csv
2016-04-13 06:21:05: Passengers Dataset: 6_7_Passengers.csv
2016-04-13 06:21:06: Route Dataset: 6_7_RouteStopsMap.csv
2016-04-13 06:21:06: Stops Dataset: 6_7_Stops.csv
2016-04-13 06:21:06: Bus Stops Distance Threshold(m): 15
2016-04-13 06:21:06: Traffic Stops Distance Threshold(m): 40
2016-04-13 06:21:06: Number Of Folds: 10
2016-04-13 06:21:06: K: 10
2016-04-13 06:21:06: Loading...
2016-04-13 06:21:06: 00 min 00 sec 963 ms
2016-04-13 06:21:06: Processing...
2016-04-13 06:21:07: Evaluating Fold 1...
2016-04-13 06:46:10: Evaluating Fold 2...
2016-04-13 07:11:44: Evaluating Fold 3...
2016-04-13 07:37:36: Evaluating Fold 4...
2016-04-13 08:05:26: Evaluating Fold 5...
2016-04-13 08:39:16: Evaluating Fold 6...
2016-04-13 09:07:09: Evaluating Fold 7...
2016-04-13 09:32:52: Evaluating Fold 8...
2016-04-13 10:00:30: Evaluating Fold 9...
2016-04-13 10:29:35: Evaluating Fold 10...
2016-04-13 15:49:21: 568 min 14 sec 630 ms
2016-04-13 15:49:21: Bus Stop Results
2016-04-13 15:49:21: Route ID:116, Precision:0.4410, Recall:0.4080, F-Measure:0.4002, Accuracy:0.5196
2016-04-13 15:49:21: Bus Stops:39 Traffic Stops:18
2016-04-13 15:49:21: Route ID:118, Precision:0.7215, Recall:0.6264, F-Measure:0.6465, Accuracy:0.6486
2016-04-13 15:49:21: Bus Stops:23 Traffic Stops:13
2016-04-13 15:49:21: Route ID:747, Precision:0.8080, Recall:0.8092, F-Measure:0.7939, Accuracy:0.8250
2016-04-13 15:49:21: Bus Stops:11300 Traffic Stops:3116
2016-04-13 15:49:21: Route ID:140, Precision:0.7412, Recall:0.7194, F-Measure:0.7210, Accuracy:0.7437
2016-04-13 15:49:21: Bus Stops:4796 Traffic Stops:2258
2016-04-13 15:49:21: Route ID:76, Precision:0.6376, Recall:0.6118, F-Measure:0.6194, Accuracy:0.7063
2016-04-13 15:49:21: Bus Stops:2479 Traffic Stops:1905
2016-04-13 15:49:21: Route ID:46, Precision:0.6391, Recall:0.6439, F-Measure:0.6349, Accuracy:0.7574
2016-04-13 15:49:21: Bus Stops:14052 Traffic Stops:11455
2016-04-13 15:49:21: Overall Results
2016-04-13 15:49:21: Precision:0.6647, Recall:0.6365, F-Measure:0.6360, Accuracy:0.7001
2016-04-13 15:49:21: Passenger Results
2016-04-13 15:49:21: Route ID:116, Precision:0.1095, Recall:0.1133, F-Measure:0.1098, Accuracy:0.6915
2016-04-13 15:49:21: Route ID:118, Precision:0.1905, Recall:0.1905, F-Measure:0.1905, Accuracy:1.0000
2016-04-13 15:49:21: Route ID:747, Precision:0.5617, Recall:0.5233, F-Measure:0.5276, Accuracy:0.9647
2016-04-13 15:49:21: Route ID:140, Precision:0.7738, Recall:0.7281, F-Measure:0.7364, Accuracy:0.9051
2016-04-13 15:49:21: Route ID:76, Precision:0.6965, Recall:0.6717, F-Measure:0.6744, Accuracy:0.9198
2016-04-13 15:49:21: Route ID:46, Precision:0.6372, Recall:0.5922, F-Measure:0.5964, Accuracy:0.8887
2016-04-13 15:49:21: Overall Results
2016-04-13 15:49:21: Precision:0.4949, Recall:0.4699, F-Measure:0.4725, Accuracy:0.8950
2016-04-13 15:49:21: 319 min 14 sec 762 ms
2016-04-13 15:49:21: Cleaning Results...
2016-04-13 15:49:22: 00 min 00 sec 252 ms
2016-04-13 15:49:22: Saving results as <Results>...
2016-04-13 15:49:22: 00 min 00 sec 96 ms
