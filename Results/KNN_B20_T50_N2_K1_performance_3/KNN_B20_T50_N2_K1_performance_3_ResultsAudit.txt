2016-04-14 12:38:46: Algorithm Selected: KNN_WITH_PW
2016-04-14 12:38:46: GPS Dataset: set_records_3_GPSPoints.csv
2016-04-14 12:38:46: Passengers Dataset: set_records_3_Passengers.csv
2016-04-14 12:38:46: Route Dataset: set_records_3_RouteStopsMap.csv
2016-04-14 12:38:46: Stops Dataset: set_records_3_Stops.csv
2016-04-14 12:38:46: Bus Stops Distance Threshold(m): 20
2016-04-14 12:38:46: Traffic Stops Distance Threshold(m): 50
2016-04-14 12:38:46: Number Of Folds: 2
2016-04-14 12:38:46: K: 1
2016-04-14 12:38:46: Loading...
2016-04-14 12:38:46: 00 min 00 sec 239 ms
2016-04-14 12:38:46: Processing...
2016-04-14 12:38:46: Evaluating Fold 1...
2016-04-14 12:46:03: Evaluating Fold 2...
2016-04-14 12:53:47: 15 min 00 sec 674 ms
2016-04-14 12:53:47: Bus Stop Results
2016-04-14 12:53:47: Route ID:1, Precision:0.6721, Recall:0.6525, F-Measure:0.6571, Accuracy:0.7019
2016-04-14 12:53:47: Bus Stops:5454 Traffic Stops:1451
2016-04-14 12:53:47: Route ID:4, Precision:0.7350, Recall:0.7226, F-Measure:0.7267, Accuracy:0.7334
2016-04-14 12:53:47: Bus Stops:6186 Traffic Stops:2565
2016-04-14 12:53:47: Route ID:7, Precision:0.5434, Recall:0.5493, F-Measure:0.5415, Accuracy:0.7072
2016-04-14 12:53:47: Bus Stops:8957 Traffic Stops:4182
2016-04-14 12:53:47: Overall Results
2016-04-14 12:53:47: Precision:0.6502, Recall:0.6415, F-Measure:0.6418, Accuracy:0.7142
2016-04-14 12:53:47: Passenger Results
2016-04-14 12:53:47: Route ID:1, Precision:0.6978, Recall:0.6910, F-Measure:0.6773, Accuracy:0.7986
2016-04-14 12:53:47: Route ID:4, Precision:0.7422, Recall:0.7201, F-Measure:0.7172, Accuracy:0.7237
2016-04-14 12:53:47: Route ID:7, Precision:0.6074, Recall:0.5875, F-Measure:0.5885, Accuracy:0.8235
2016-04-14 12:53:47: Overall Results
2016-04-14 12:53:47: Precision:0.6824, Recall:0.6662, F-Measure:0.6610, Accuracy:0.7819
2016-04-14 12:53:47: 15 min 00 sec 778 ms
2016-04-14 12:53:47: Cleaning Results...
2016-04-14 12:53:47: 00 min 00 sec 116 ms
2016-04-14 12:53:47: Saving results as <Results>...
2016-04-14 12:53:47: 00 min 00 sec 125 ms
