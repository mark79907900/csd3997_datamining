2016-04-15 02:23:27: Algorithm Selected: KNN_WITH_PW
2016-04-15 02:23:27: GPS Dataset: set_records_8_GPSPoints.csv
2016-04-15 02:23:27: Passengers Dataset: set_records_8_Passengers.csv
2016-04-15 02:23:27: Route Dataset: set_records_8_RouteStopsMap.csv
2016-04-15 02:23:27: Stops Dataset: set_records_8_Stops.csv
2016-04-15 02:23:27: Bus Stops Distance Threshold(m): 20
2016-04-15 02:23:27: Traffic Stops Distance Threshold(m): 50
2016-04-15 02:23:27: Number Of Folds: 2
2016-04-15 02:23:27: K: 1
2016-04-15 02:23:27: Loading...
2016-04-15 02:23:29: 00 min 01 sec 212 ms
2016-04-15 02:23:29: Processing...
2016-04-15 02:23:29: Evaluating Fold 1...
2016-04-15 05:09:34: Evaluating Fold 2...
2016-04-15 07:25:21: 301 min 52 sec 649 ms
2016-04-15 07:25:21: Bus Stop Results
2016-04-15 07:25:21: Route ID:1, Precision:0.6859, Recall:0.6530, F-Measure:0.6654, Accuracy:0.6965
2016-04-15 07:25:21: Bus Stops:5581 Traffic Stops:1512
2016-04-15 07:25:21: Route ID:4, Precision:0.7401, Recall:0.7125, F-Measure:0.7229, Accuracy:0.7116
2016-04-15 07:25:21: Bus Stops:6317 Traffic Stops:2421
2016-04-15 07:25:21: Route ID:7, Precision:0.5400, Recall:0.5293, F-Measure:0.5265, Accuracy:0.6900
2016-04-15 07:25:21: Bus Stops:8861 Traffic Stops:4291
2016-04-15 07:25:21: Route ID:9, Precision:0.6642, Recall:0.6438, F-Measure:0.6501, Accuracy:0.6684
2016-04-15 07:25:21: Bus Stops:8447 Traffic Stops:2655
2016-04-15 07:25:21: Route ID:11, Precision:0.6084, Recall:0.5701, F-Measure:0.5798, Accuracy:0.6446
2016-04-15 07:25:21: Bus Stops:5651 Traffic Stops:1728
2016-04-15 07:25:21: Route ID:13, Precision:0.7003, Recall:0.6844, F-Measure:0.6893, Accuracy:0.7040
2016-04-15 07:25:21: Bus Stops:9144 Traffic Stops:4068
2016-04-15 07:25:21: Route ID:14, Precision:0.6101, Recall:0.5972, F-Measure:0.5991, Accuracy:0.6840
2016-04-15 07:25:21: Bus Stops:9879 Traffic Stops:4114
2016-04-15 07:25:21: Route ID:15, Precision:0.6798, Recall:0.6747, F-Measure:0.6740, Accuracy:0.7453
2016-04-15 07:25:21: Bus Stops:22317 Traffic Stops:13001
2016-04-15 07:25:21: Overall Results
2016-04-15 07:25:21: Precision:0.6536, Recall:0.6331, F-Measure:0.6384, Accuracy:0.6931
2016-04-15 07:25:21: Passenger Results
2016-04-15 07:25:21: Route ID:1, Precision:0.7346, Recall:0.7436, F-Measure:0.7306, Accuracy:0.7998
2016-04-15 07:25:21: Route ID:4, Precision:0.7741, Recall:0.7351, F-Measure:0.7376, Accuracy:0.7162
2016-04-15 07:25:21: Route ID:7, Precision:0.5998, Recall:0.5879, F-Measure:0.5839, Accuracy:0.8082
2016-04-15 07:25:21: Route ID:9, Precision:0.7088, Recall:0.6781, F-Measure:0.6780, Accuracy:0.8589
2016-04-15 07:25:21: Route ID:11, Precision:0.7635, Recall:0.7202, F-Measure:0.7199, Accuracy:0.8683
2016-04-15 07:25:21: Route ID:13, Precision:0.7249, Recall:0.6875, F-Measure:0.6849, Accuracy:0.7390
2016-04-15 07:25:21: Route ID:14, Precision:0.6146, Recall:0.5852, F-Measure:0.5803, Accuracy:0.7441
2016-04-15 07:25:21: Route ID:15, Precision:0.6267, Recall:0.5915, F-Measure:0.5905, Accuracy:0.7508
2016-04-15 07:25:22: Overall Results
2016-04-15 07:25:22: Precision:0.6934, Recall:0.6661, F-Measure:0.6632, Accuracy:0.7857
2016-04-15 07:25:22: 301 min 52 sec 899 ms
2016-04-15 07:25:22: Cleaning Results...
2016-04-15 07:25:22: 00 min 00 sec 374 ms
2016-04-15 07:25:22: Saving results as <Results>...
2016-04-15 07:25:22: 00 min 00 sec 205 ms
