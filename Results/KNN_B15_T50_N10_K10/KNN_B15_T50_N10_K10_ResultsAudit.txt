2016-04-11 18:06:14: Algorithm Selected: KNN_WITH_PW
2016-04-11 18:06:14: GPS Dataset: 6_7_GPSPoints.csv
2016-04-11 18:06:14: Passengers Dataset: 6_7_Passengers.csv
2016-04-11 18:06:14: Route Dataset: 6_7_RouteStopsMap.csv
2016-04-11 18:06:14: Stops Dataset: 6_7_Stops.csv
2016-04-11 18:06:14: Bus Stops Distance Threshold(m): 15
2016-04-11 18:06:14: Traffic Stops Distance Threshold(m): 50
2016-04-11 18:06:14: Number Of Folds: 10
2016-04-11 18:06:14: K: 10
2016-04-11 18:06:14: Loading...
2016-04-11 18:06:16: 00 min 01 sec 334 ms
2016-04-11 18:06:16: Processing...
2016-04-11 18:06:16: Evaluating Fold 1...
2016-04-11 18:35:55: Evaluating Fold 2...
2016-04-11 19:05:19: Evaluating Fold 3...
2016-04-11 19:34:59: Evaluating Fold 4...
2016-04-11 20:04:39: Evaluating Fold 5...
2016-04-11 20:34:24: Evaluating Fold 6...
2016-04-11 21:04:17: Evaluating Fold 7...
2016-04-11 21:35:16: Evaluating Fold 8...
2016-04-11 22:06:32: Evaluating Fold 9...
2016-04-11 22:39:28: Evaluating Fold 10...
2016-04-11 23:13:49: 307 min 33 sec 247 ms
2016-04-11 23:13:49: Bus Stop Results
2016-04-11 23:13:49: Route ID:116, Precision:0.4350, Recall:0.4003, F-Measure:0.3906, Accuracy:0.5080
2016-04-11 23:13:49: Bus Stops:36 Traffic Stops:21
2016-04-11 23:13:49: Route ID:118, Precision:0.7240, Recall:0.6407, F-Measure:0.6533, Accuracy:0.6534
2016-04-11 23:13:49: Bus Stops:23 Traffic Stops:19
2016-04-11 23:13:49: Route ID:747, Precision:0.8166, Recall:0.8032, F-Measure:0.7949, Accuracy:0.8346
2016-04-11 23:13:49: Bus Stops:11628 Traffic Stops:4523
2016-04-11 23:13:49: Route ID:140, Precision:0.7410, Recall:0.7170, F-Measure:0.7202, Accuracy:0.7446
2016-04-11 23:13:49: Bus Stops:4637 Traffic Stops:4662
2016-04-11 23:13:49: Route ID:76, Precision:0.6419, Recall:0.6179, F-Measure:0.6247, Accuracy:0.7118
2016-04-11 23:13:49: Bus Stops:2562 Traffic Stops:2192
2016-04-11 23:13:49: Route ID:46, Precision:0.6358, Recall:0.6392, F-Measure:0.6304, Accuracy:0.7584
2016-04-11 23:13:49: Bus Stops:13166 Traffic Stops:17115
2016-04-11 23:13:49: Overall Results
2016-04-11 23:13:49: Precision:0.6657, Recall:0.6364, F-Measure:0.6357, Accuracy:0.7018
2016-04-11 23:13:49: Passenger Results
2016-04-11 23:13:49: Route ID:116, Precision:0.1000, Recall:0.1038, F-Measure:0.1002, Accuracy:0.6742
2016-04-11 23:13:49: Route ID:118, Precision:0.2143, Recall:0.2143, F-Measure:0.2143, Accuracy:1.0000
2016-04-11 23:13:49: Route ID:747, Precision:0.5985, Recall:0.5605, F-Measure:0.5643, Accuracy:0.9650
2016-04-11 23:13:49: Route ID:140, Precision:0.7723, Recall:0.7148, F-Measure:0.7298, Accuracy:0.9103
2016-04-11 23:13:49: Route ID:76, Precision:0.7310, Recall:0.6987, F-Measure:0.6978, Accuracy:0.9302
2016-04-11 23:13:49: Route ID:46, Precision:0.6346, Recall:0.5836, F-Measure:0.5888, Accuracy:0.8902
2016-04-11 23:13:49: Overall Results
2016-04-11 23:13:49: Precision:0.5084, Recall:0.4793, F-Measure:0.4825, Accuracy:0.8950
2016-04-11 23:13:49: 307 min 33 sec 421 ms
2016-04-11 23:13:49: Cleaning Results...
2016-04-11 23:13:49: 00 min 00 sec 312 ms
2016-04-11 23:13:49: Saving results as <Results>...
2016-04-11 23:13:50: 00 min 00 sec 55 ms
