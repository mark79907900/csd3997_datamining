2016-04-13 16:24:15: Algorithm Selected: KNN_WITH_PW
2016-04-13 16:24:15: GPS Dataset: 6_7_GPSPoints.csv
2016-04-13 16:24:15: Passengers Dataset: 6_7_Passengers.csv
2016-04-13 16:24:15: Route Dataset: 6_7_RouteStopsMap.csv
2016-04-13 16:24:15: Stops Dataset: 6_7_Stops.csv
2016-04-13 16:24:15: Bus Stops Distance Threshold(m): 20
2016-04-13 16:24:15: Traffic Stops Distance Threshold(m): 50
2016-04-13 16:24:15: Number Of Folds: 10
2016-04-13 16:24:15: K: 15
2016-04-13 16:24:15: Loading...
2016-04-13 16:24:16: 00 min 00 sec 946 ms
2016-04-13 16:24:16: Processing...
2016-04-13 16:24:16: Evaluating Fold 1...
2016-04-13 16:47:33: Evaluating Fold 2...
2016-04-13 17:11:56: Evaluating Fold 3...
2016-04-13 17:36:59: Evaluating Fold 4...
2016-04-13 18:02:04: Evaluating Fold 5...
2016-04-13 18:27:08: Evaluating Fold 6...
2016-04-13 18:52:16: Evaluating Fold 7...
2016-04-13 19:18:33: Evaluating Fold 8...
2016-04-13 19:44:27: Evaluating Fold 9...
2016-04-13 20:09:31: Evaluating Fold 10...
2016-04-13 20:36:09: 251 min 52 sec 892 ms
2016-04-13 20:36:09: Bus Stop Results
2016-04-13 20:36:09: Route ID:116, Precision:0.4448, Recall:0.4148, F-Measure:0.4034, Accuracy:0.5207
2016-04-13 20:36:09: Bus Stops:42 Traffic Stops:19
2016-04-13 20:36:09: Route ID:118, Precision:0.7367, Recall:0.6340, F-Measure:0.6524, Accuracy:0.6462
2016-04-13 20:36:09: Bus Stops:36 Traffic Stops:8
2016-04-13 20:36:09: Route ID:747, Precision:0.8125, Recall:0.8157, F-Measure:0.8011, Accuracy:0.8145
2016-04-13 20:36:09: Bus Stops:12410 Traffic Stops:2775
2016-04-13 20:36:09: Route ID:76, Precision:0.6283, Recall:0.6029, F-Measure:0.6090, Accuracy:0.6975
2016-04-13 20:36:09: Bus Stops:3071 Traffic Stops:1449
2016-04-13 20:36:09: Route ID:140, Precision:0.7352, Recall:0.7052, F-Measure:0.7101, Accuracy:0.7327
2016-04-13 20:36:09: Bus Stops:4634 Traffic Stops:4137
2016-04-13 20:36:09: Route ID:46, Precision:0.6259, Recall:0.6294, F-Measure:0.6199, Accuracy:0.7424
2016-04-13 20:36:09: Bus Stops:14811 Traffic Stops:13821
2016-04-13 20:36:09: Overall Results
2016-04-13 20:36:09: Precision:0.6639, Recall:0.6337, F-Measure:0.6326, Accuracy:0.6923
2016-04-13 20:36:09: Passenger Results
2016-04-13 20:36:09: Route ID:116, Precision:0.1190, Recall:0.1229, F-Measure:0.1193, Accuracy:0.6979
2016-04-13 20:36:09: Route ID:118, Precision:0.2714, Recall:0.2857, F-Measure:0.2755, Accuracy:0.8929
2016-04-13 20:36:09: Route ID:747, Precision:0.5627, Recall:0.5524, F-Measure:0.5393, Accuracy:0.9628
2016-04-13 20:36:09: Route ID:76, Precision:0.7405, Recall:0.6656, F-Measure:0.6748, Accuracy:0.9224
2016-04-13 20:36:09: Route ID:140, Precision:0.7663, Recall:0.6839, F-Measure:0.6994, Accuracy:0.8591
2016-04-13 20:36:09: Route ID:46, Precision:0.5997, Recall:0.5498, F-Measure:0.5532, Accuracy:0.8719
2016-04-13 20:36:09: Overall Results
2016-04-13 20:36:09: Precision:0.5100, Recall:0.4767, F-Measure:0.4769, Accuracy:0.8678
2016-04-13 20:36:09: 251 min 53 sec 55 ms
2016-04-13 20:36:09: Cleaning Results...
2016-04-13 20:36:09: 00 min 00 sec 167 ms
2016-04-13 20:36:09: Saving results as <Results>...
2016-04-13 20:36:09: 00 min 00 sec 94 ms
