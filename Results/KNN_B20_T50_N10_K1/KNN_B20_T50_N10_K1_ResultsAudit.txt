2016-04-12 00:19:58: Algorithm Selected: KNN_WITH_PW
2016-04-12 00:19:58: GPS Dataset: 6_7_GPSPoints.csv
2016-04-12 00:19:58: Passengers Dataset: 6_7_Passengers.csv
2016-04-12 00:19:58: Route Dataset: 6_7_RouteStopsMap.csv
2016-04-12 00:19:58: Stops Dataset: 6_7_Stops.csv
2016-04-12 00:19:58: Bus Stops Distance Threshold(m): 20
2016-04-12 00:19:58: Traffic Stops Distance Threshold(m): 50
2016-04-12 00:19:58: Number Of Folds: 10
2016-04-12 00:19:58: K: 1
2016-04-12 00:19:58: Loading...
2016-04-12 00:20:03: 00 min 04 sec 475 ms
2016-04-12 00:20:03: Processing...
2016-04-12 00:20:03: Evaluating Fold 1...
2016-04-12 00:52:33: Evaluating Fold 2...
2016-04-12 01:23:53: Evaluating Fold 3...
2016-04-12 01:56:04: Evaluating Fold 4...
2016-04-12 02:28:48: Evaluating Fold 5...
2016-04-12 03:01:04: Evaluating Fold 6...
2016-04-12 03:32:32: Evaluating Fold 7...
2016-04-12 04:03:26: Evaluating Fold 8...
2016-04-12 04:34:05: Evaluating Fold 9...
2016-04-12 05:05:29: Evaluating Fold 10...
2016-04-12 05:38:23: 318 min 20 sec 262 ms
2016-04-12 05:38:23: Bus Stop Results
2016-04-12 05:38:23: Route ID:116, Precision:0.4910, Recall:0.4760, F-Measure:0.4751, Accuracy:0.5843
2016-04-12 05:38:23: Bus Stops:68 Traffic Stops:17
2016-04-12 05:38:23: Route ID:118, Precision:0.7924, Recall:0.7117, F-Measure:0.7348, Accuracy:0.7101
2016-04-12 05:38:23: Bus Stops:57 Traffic Stops:9
2016-04-12 05:38:23: Route ID:747, Precision:0.8260, Recall:0.8106, F-Measure:0.8153, Accuracy:0.8759
2016-04-12 05:38:23: Bus Stops:14652 Traffic Stops:2984
2016-04-12 05:38:23: Route ID:140, Precision:0.7689, Recall:0.7486, F-Measure:0.7550, Accuracy:0.7772
2016-04-12 05:38:23: Bus Stops:5960 Traffic Stops:3460
2016-04-12 05:38:23: Route ID:76, Precision:0.6621, Recall:0.6596, F-Measure:0.6588, Accuracy:0.7577
2016-04-12 05:38:23: Bus Stops:3928 Traffic Stops:1519
2016-04-12 05:38:23: Route ID:46, Precision:0.6826, Recall:0.6821, F-Measure:0.6802, Accuracy:0.8196
2016-04-12 05:38:23: Bus Stops:23243 Traffic Stops:15997
2016-04-12 05:38:23: Overall Results
2016-04-12 05:38:23: Precision:0.7038, Recall:0.6814, F-Measure:0.6865, Accuracy:0.7541
2016-04-12 05:38:23: Passenger Results
2016-04-12 05:38:23: Route ID:116, Precision:0.1825, Recall:0.1894, F-Measure:0.1841, Accuracy:0.7815
2016-04-12 05:38:23: Route ID:118, Precision:0.4143, Recall:0.4286, F-Measure:0.4184, Accuracy:0.9167
2016-04-12 05:38:23: Route ID:747, Precision:0.5411, Recall:0.5291, F-Measure:0.5230, Accuracy:0.9600
2016-04-12 05:38:23: Route ID:140, Precision:0.7383, Recall:0.7106, F-Measure:0.7099, Accuracy:0.8614
2016-04-12 05:38:23: Route ID:76, Precision:0.7529, Recall:0.7187, F-Measure:0.7229, Accuracy:0.9243
2016-04-12 05:38:23: Route ID:46, Precision:0.6448, Recall:0.6104, F-Measure:0.6156, Accuracy:0.8603
2016-04-12 05:38:23: Overall Results
2016-04-12 05:38:23: Precision:0.5457, Recall:0.5311, F-Measure:0.5290, Accuracy:0.8840
2016-04-12 05:38:23: 318 min 20 sec 447 ms
2016-04-12 05:38:23: Cleaning Results...
2016-04-12 05:38:23: 00 min 00 sec 194 ms
2016-04-12 05:38:23: Saving results as <Results>...
2016-04-12 05:38:24: 00 min 00 sec 184 ms
