2016-04-14 06:19:38: Algorithm Selected: PARSON_WINDOWS
2016-04-14 06:19:38: GPS Dataset: 6_7_GPSPoints.csv
2016-04-14 06:19:38: Passengers Dataset: 6_7_Passengers.csv
2016-04-14 06:19:38: Route Dataset: 6_7_RouteStopsMap.csv
2016-04-14 06:19:38: Stops Dataset: 6_7_Stops.csv
2016-04-14 06:19:38: Bus Stops Distance Threshold(m): 20
2016-04-14 06:19:38: Traffic Stops Distance Threshold(m): 40
2016-04-14 06:19:38: Loading...
2016-04-14 06:19:40: 00 min 01 sec 378 ms
2016-04-14 06:19:40: Processing...
2016-04-14 06:19:40: Evaluating Route 46...
2016-04-14 06:22:10: Evaluating Route 76...
2016-04-14 06:22:19: Evaluating Route 116...
2016-04-14 06:22:19: Evaluating Route 118...
2016-04-14 06:22:20: Evaluating Route 140...
2016-04-14 06:22:35: Evaluating Route 747...
2016-04-14 06:22:57: Bus Stop Results
2016-04-14 06:22:57: Route ID:116, Precision:0.4554, Recall:0.4666, F-Measure:0.4357, Accuracy:0.5315
2016-04-14 06:22:57: Bus Stops:161 Traffic Stops:93
2016-04-14 06:22:57: Route ID:118, Precision:0.5621, Recall:0.5335, F-Measure:0.5268, Accuracy:0.7386
2016-04-14 06:22:57: Bus Stops:69 Traffic Stops:19
2016-04-14 06:22:57: Route ID:747, Precision:0.3593, Recall:0.4136, F-Measure:0.3550, Accuracy:0.7781
2016-04-14 06:22:57: Bus Stops:16560 Traffic Stops:3873
2016-04-14 06:22:57: Route ID:76, Precision:0.3645, Recall:0.3850, F-Measure:0.3505, Accuracy:0.5014
2016-04-14 06:22:57: Bus Stops:5617 Traffic Stops:4147
2016-04-14 06:22:57: Route ID:140, Precision:0.4668, Recall:0.4781, F-Measure:0.4610, Accuracy:0.5019
2016-04-14 06:22:57: Bus Stops:9337 Traffic Stops:6348
2016-04-14 06:22:57: Route ID:46, Precision:0.3729, Recall:0.3973, F-Measure:0.3655, Accuracy:0.5271
2016-04-14 06:22:57: Bus Stops:35404 Traffic Stops:28235
2016-04-14 06:22:57: Overall Results
2016-04-14 06:22:57: Precision:0.4302, Recall:0.4457, F-Measure:0.4158, Accuracy:0.5964
2016-04-14 06:22:57: Passenger Results
2016-04-14 06:22:57: Route ID:116, Precision:0.3849, Recall:0.3858, F-Measure:0.3793, Accuracy:0.7671
2016-04-14 06:22:57: Route ID:118, Precision:0.4921, Recall:0.4881, F-Measure:0.4856, Accuracy:0.9053
2016-04-14 06:22:57: Route ID:747, Precision:0.6142, Recall:0.5726, F-Measure:0.5735, Accuracy:0.9535
2016-04-14 06:22:57: Route ID:76, Precision:0.7160, Recall:0.7273, F-Measure:0.7141, Accuracy:0.8942
2016-04-14 06:22:57: Route ID:140, Precision:0.7544, Recall:0.7326, F-Measure:0.7356, Accuracy:0.8264
2016-04-14 06:22:57: Route ID:46, Precision:0.6527, Recall:0.6394, F-Measure:0.6410, Accuracy:0.8244
2016-04-14 06:22:57: Overall Results
2016-04-14 06:22:57: Precision:0.6024, Recall:0.5910, F-Measure:0.5882, Accuracy:0.8618
2016-04-14 06:22:57: 03 min 17 sec 614 ms
2016-04-14 06:22:57: Cleaning Results...
2016-04-14 06:22:57: 00 min 00 sec 133 ms
2016-04-14 06:22:57: Saving results as <Results>...
2016-04-14 06:22:58: 00 min 00 sec 101 ms
