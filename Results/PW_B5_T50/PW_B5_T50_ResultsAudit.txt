2016-04-14 06:04:26: Algorithm Selected: PARSON_WINDOWS
2016-04-14 06:04:26: GPS Dataset: 6_7_GPSPoints.csv
2016-04-14 06:04:26: Passengers Dataset: 6_7_Passengers.csv
2016-04-14 06:04:26: Route Dataset: 6_7_RouteStopsMap.csv
2016-04-14 06:04:26: Stops Dataset: 6_7_Stops.csv
2016-04-14 06:04:26: Bus Stops Distance Threshold(m): 5
2016-04-14 06:04:26: Traffic Stops Distance Threshold(m): 50
2016-04-14 06:04:26: Loading...
2016-04-14 06:04:27: 00 min 00 sec 995 ms
2016-04-14 06:04:27: Processing...
2016-04-14 06:04:27: Evaluating Route 46...
2016-04-14 06:06:02: Evaluating Route 76...
2016-04-14 06:06:10: Evaluating Route 116...
2016-04-14 06:06:10: Evaluating Route 118...
2016-04-14 06:06:10: Evaluating Route 140...
2016-04-14 06:06:20: Evaluating Route 747...
2016-04-14 06:06:29: Bus Stop Results
2016-04-14 06:06:29: Route ID:116, Precision:0.4826, Recall:0.4857, F-Measure:0.4541, Accuracy:0.5560
2016-04-14 06:06:29: Bus Stops:51 Traffic Stops:199
2016-04-14 06:06:29: Route ID:118, Precision:0.5236, Recall:0.5193, F-Measure:0.4954, Accuracy:0.6869
2016-04-14 06:06:29: Bus Stops:8 Traffic Stops:91
2016-04-14 06:06:29: Route ID:747, Precision:0.3656, Recall:0.4182, F-Measure:0.3659, Accuracy:0.7854
2016-04-14 06:06:29: Bus Stops:2653 Traffic Stops:19260
2016-04-14 06:06:29: Route ID:76, Precision:0.3848, Recall:0.4058, F-Measure:0.3736, Accuracy:0.5200
2016-04-14 06:06:29: Bus Stops:1366 Traffic Stops:8302
2016-04-14 06:06:29: Route ID:140, Precision:0.4442, Recall:0.4584, F-Measure:0.4379, Accuracy:0.4801
2016-04-14 06:06:29: Bus Stops:2057 Traffic Stops:17081
2016-04-14 06:06:29: Route ID:46, Precision:0.3856, Recall:0.4094, F-Measure:0.3771, Accuracy:0.5478
2016-04-14 06:06:29: Bus Stops:5951 Traffic Stops:59956
2016-04-14 06:06:29: Overall Results
2016-04-14 06:06:29: Precision:0.4311, Recall:0.4495, F-Measure:0.4173, Accuracy:0.5960
2016-04-14 06:06:29: Passenger Results
2016-04-14 06:06:29: Route ID:116, Precision:0.1175, Recall:0.1214, F-Measure:0.1187, Accuracy:0.7143
2016-04-14 06:06:29: Route ID:118, Precision:0.0714, Recall:0.0714, F-Measure:0.0714, Accuracy:1.0000
2016-04-14 06:06:29: Route ID:747, Precision:0.4292, Recall:0.4191, F-Measure:0.4067, Accuracy:0.9699
2016-04-14 06:06:29: Route ID:76, Precision:0.2982, Recall:0.2972, F-Measure:0.2912, Accuracy:0.9431
2016-04-14 06:06:29: Route ID:140, Precision:0.2778, Recall:0.2836, F-Measure:0.2792, Accuracy:0.9778
2016-04-14 06:06:29: Route ID:46, Precision:0.3391, Recall:0.3474, F-Measure:0.3389, Accuracy:0.9380
2016-04-14 06:06:29: Overall Results
2016-04-14 06:06:29: Precision:0.2555, Recall:0.2567, F-Measure:0.2510, Accuracy:0.9238
2016-04-14 06:06:29: 02 min 02 sec 249 ms
2016-04-14 06:06:29: Cleaning Results...
2016-04-14 06:06:29: 00 min 00 sec 46 ms
2016-04-14 06:06:29: Saving results as <Results>...
2016-04-14 06:06:29: 00 min 00 sec 44 ms
