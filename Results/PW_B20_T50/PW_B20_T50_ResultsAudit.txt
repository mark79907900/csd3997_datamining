2016-04-14 06:23:34: Algorithm Selected: PARSON_WINDOWS
2016-04-14 06:23:34: GPS Dataset: 6_7_GPSPoints.csv
2016-04-14 06:23:34: Passengers Dataset: 6_7_Passengers.csv
2016-04-14 06:23:34: Route Dataset: 6_7_RouteStopsMap.csv
2016-04-14 06:23:34: Stops Dataset: 6_7_Stops.csv
2016-04-14 06:23:34: Bus Stops Distance Threshold(m): 20
2016-04-14 06:23:34: Traffic Stops Distance Threshold(m): 50
2016-04-14 06:23:34: Loading...
2016-04-14 06:23:35: 00 min 00 sec 900 ms
2016-04-14 06:23:35: Processing...
2016-04-14 06:23:35: Evaluating Route 46...
2016-04-14 06:25:51: Evaluating Route 76...
2016-04-14 06:25:59: Evaluating Route 116...
2016-04-14 06:26:00: Evaluating Route 118...
2016-04-14 06:26:00: Evaluating Route 140...
2016-04-14 06:26:10: Evaluating Route 747...
2016-04-14 06:26:26: Bus Stop Results
2016-04-14 06:26:26: Route ID:116, Precision:0.4207, Recall:0.4453, F-Measure:0.4100, Accuracy:0.4826
2016-04-14 06:26:26: Bus Stops:161 Traffic Stops:127
2016-04-14 06:26:26: Route ID:118, Precision:0.5236, Recall:0.5193, F-Measure:0.4954, Accuracy:0.6869
2016-04-14 06:26:26: Bus Stops:69 Traffic Stops:30
2016-04-14 06:26:26: Route ID:747, Precision:0.3525, Recall:0.4038, F-Measure:0.3516, Accuracy:0.7682
2016-04-14 06:26:26: Bus Stops:16560 Traffic Stops:5847
2016-04-14 06:26:26: Route ID:76, Precision:0.3488, Recall:0.3722, F-Measure:0.3376, Accuracy:0.4762
2016-04-14 06:26:26: Bus Stops:5617 Traffic Stops:5205
2016-04-14 06:26:26: Route ID:140, Precision:0.4356, Recall:0.4516, F-Measure:0.4315, Accuracy:0.4709
2016-04-14 06:26:26: Bus Stops:9337 Traffic Stops:10515
2016-04-14 06:26:26: Route ID:46, Precision:0.3549, Recall:0.3768, F-Measure:0.3491, Accuracy:0.5123
2016-04-14 06:26:26: Bus Stops:35404 Traffic Stops:38575
2016-04-14 06:26:26: Overall Results
2016-04-14 06:26:26: Precision:0.4060, Recall:0.4282, F-Measure:0.3959, Accuracy:0.5662
2016-04-14 06:26:26: Passenger Results
2016-04-14 06:26:26: Route ID:116, Precision:0.3468, Recall:0.3477, F-Measure:0.3412, Accuracy:0.6982
2016-04-14 06:26:26: Route ID:118, Precision:0.4921, Recall:0.4881, F-Measure:0.4856, Accuracy:0.9053
2016-04-14 06:26:26: Route ID:747, Precision:0.6105, Recall:0.5748, F-Measure:0.5749, Accuracy:0.9537
2016-04-14 06:26:26: Route ID:76, Precision:0.6734, Recall:0.6825, F-Measure:0.6693, Accuracy:0.8955
2016-04-14 06:26:26: Route ID:140, Precision:0.7419, Recall:0.7142, F-Measure:0.7183, Accuracy:0.8278
2016-04-14 06:26:26: Route ID:46, Precision:0.6334, Recall:0.6247, F-Measure:0.6216, Accuracy:0.8261
2016-04-14 06:26:26: Overall Results
2016-04-14 06:26:26: Precision:0.5830, Recall:0.5720, F-Measure:0.5685, Accuracy:0.8511
2016-04-14 06:26:26: 02 min 51 sec 513 ms
2016-04-14 06:26:26: Cleaning Results...
2016-04-14 06:26:26: 00 min 00 sec 122 ms
2016-04-14 06:26:26: Saving results as <Results>...
2016-04-14 06:26:26: 00 min 00 sec 90 ms
