2016-04-15 08:32:57: Algorithm Selected: PARSON_WINDOWS
2016-04-15 08:32:57: GPS Dataset: set_records_8_GPSPoints.csv
2016-04-15 08:32:57: Passengers Dataset: set_records_8_Passengers.csv
2016-04-15 08:32:57: Route Dataset: set_records_8_RouteStopsMap.csv
2016-04-15 08:32:57: Stops Dataset: set_records_8_Stops.csv
2016-04-15 08:32:57: Bus Stops Distance Threshold(m): 20
2016-04-15 08:32:57: Traffic Stops Distance Threshold(m): 50
2016-04-15 08:32:57: Loading...
2016-04-15 08:33:00: 00 min 02 sec 680 ms
2016-04-15 08:33:00: Processing...
2016-04-15 08:33:00: Evaluating Route 1...
2016-04-15 08:33:06: Evaluating Route 4...
2016-04-15 08:33:16: Evaluating Route 7...
2016-04-15 08:33:49: Evaluating Route 9...
2016-04-15 08:34:05: Evaluating Route 11...
2016-04-15 08:34:11: Evaluating Route 13...
2016-04-15 08:34:43: Evaluating Route 14...
2016-04-15 08:35:13: Evaluating Route 15...
2016-04-15 08:36:42: Bus Stop Results
2016-04-15 08:36:42: Route ID:1, Precision:0.4224, Recall:0.4230, F-Measure:0.4104, Accuracy:0.5301
2016-04-15 08:36:42: Bus Stops:8364 Traffic Stops:4453
2016-04-15 08:36:42: Route ID:4, Precision:0.4111, Recall:0.4231, F-Measure:0.4055, Accuracy:0.4309
2016-04-15 08:36:42: Bus Stops:9855 Traffic Stops:9365
2016-04-15 08:36:42: Route ID:7, Precision:0.3681, Recall:0.4244, F-Measure:0.3722, Accuracy:0.4527
2016-04-15 08:36:42: Bus Stops:13413 Traffic Stops:12251
2016-04-15 08:36:42: Route ID:9, Precision:0.3713, Recall:0.3891, F-Measure:0.3694, Accuracy:0.4716
2016-04-15 08:36:42: Bus Stops:12309 Traffic Stops:9656
2016-04-15 08:36:42: Route ID:11, Precision:0.3681, Recall:0.3897, F-Measure:0.3667, Accuracy:0.4878
2016-04-15 08:36:42: Bus Stops:8608 Traffic Stops:5561
2016-04-15 08:36:42: Route ID:13, Precision:0.3720, Recall:0.3848, F-Measure:0.3661, Accuracy:0.4366
2016-04-15 08:36:42: Bus Stops:14339 Traffic Stops:14077
2016-04-15 08:36:42: Route ID:14, Precision:0.3322, Recall:0.3460, F-Measure:0.3220, Accuracy:0.3921
2016-04-15 08:36:42: Bus Stops:17438 Traffic Stops:14405
2016-04-15 08:36:42: Route ID:15, Precision:0.3615, Recall:0.3870, F-Measure:0.3587, Accuracy:0.4666
2016-04-15 08:36:42: Bus Stops:36136 Traffic Stops:34627
2016-04-15 08:36:42: Overall Results
2016-04-15 08:36:42: Precision:0.3759, Recall:0.3959, F-Measure:0.3714, Accuracy:0.4586
2016-04-15 08:36:42: Passenger Results
2016-04-15 08:36:42: Route ID:1, Precision:0.7111, Recall:0.7138, F-Measure:0.7068, Accuracy:0.7456
2016-04-15 08:36:42: Route ID:4, Precision:0.7062, Recall:0.6982, F-Measure:0.6922, Accuracy:0.8204
2016-04-15 08:36:42: Route ID:7, Precision:0.6024, Recall:0.5963, F-Measure:0.5924, Accuracy:0.7626
2016-04-15 08:36:42: Route ID:9, Precision:0.6676, Recall:0.6587, F-Measure:0.6550, Accuracy:0.8327
2016-04-15 08:36:42: Route ID:11, Precision:0.7097, Recall:0.7238, F-Measure:0.7059, Accuracy:0.8305
2016-04-15 08:36:42: Route ID:13, Precision:0.6726, Recall:0.6597, F-Measure:0.6539, Accuracy:0.7391
2016-04-15 08:36:42: Route ID:14, Precision:0.6677, Recall:0.6385, F-Measure:0.6329, Accuracy:0.8340
2016-04-15 08:36:42: Route ID:15, Precision:0.6005, Recall:0.5935, F-Measure:0.5856, Accuracy:0.6878
2016-04-15 08:36:42: Overall Results
2016-04-15 08:36:42: Precision:0.6672, Recall:0.6603, F-Measure:0.6531, Accuracy:0.7816
2016-04-15 08:36:42: 03 min 41 sec 865 ms
2016-04-15 08:36:42: Cleaning Results...
2016-04-15 08:36:42: 00 min 00 sec 113 ms
2016-04-15 08:36:42: Saving results as <Results>...
2016-04-15 08:36:42: 00 min 00 sec 81 ms
