2016-04-13 16:23:25: Algorithm Selected: KNN_WITH_PW
2016-04-13 16:23:25: GPS Dataset: 6_7_GPSPoints.csv
2016-04-13 16:23:25: Passengers Dataset: 6_7_Passengers.csv
2016-04-13 16:23:25: Route Dataset: 6_7_RouteStopsMap.csv
2016-04-13 16:23:25: Stops Dataset: 6_7_Stops.csv
2016-04-13 16:23:25: Bus Stops Distance Threshold(m): 20
2016-04-13 16:23:25: Traffic Stops Distance Threshold(m): 40
2016-04-13 16:23:25: Number Of Folds: 10
2016-04-13 16:23:25: K: 10
2016-04-13 16:23:25: Loading...
2016-04-13 16:23:26: 00 min 00 sec 987 ms
2016-04-13 16:23:26: Processing...
2016-04-13 16:23:26: Evaluating Fold 1...
2016-04-13 16:48:53: Evaluating Fold 2...
2016-04-13 17:15:35: Evaluating Fold 3...
2016-04-13 17:42:51: Evaluating Fold 4...
2016-04-13 18:10:05: Evaluating Fold 5...
2016-04-13 18:37:23: Evaluating Fold 6...
2016-04-13 19:05:09: Evaluating Fold 7...
2016-04-13 19:33:48: Evaluating Fold 8...
2016-04-13 20:01:24: Evaluating Fold 9...
2016-04-13 20:28:42: Evaluating Fold 10...
2016-04-13 21:00:27: 277 min 00 sec 453 ms
2016-04-13 21:00:27: Bus Stop Results
2016-04-13 21:00:27: Route ID:116, Precision:0.4421, Recall:0.4124, F-Measure:0.4057, Accuracy:0.5230
2016-04-13 21:00:27: Bus Stops:45 Traffic Stops:13
2016-04-13 21:00:27: Route ID:118, Precision:0.7281, Recall:0.6516, F-Measure:0.6635, Accuracy:0.6606
2016-04-13 21:00:27: Bus Stops:37 Traffic Stops:3
2016-04-13 21:00:27: Route ID:747, Precision:0.8163, Recall:0.8029, F-Measure:0.7964, Accuracy:0.8274
2016-04-13 21:00:27: Bus Stops:12899 Traffic Stops:1594
2016-04-13 21:00:27: Route ID:140, Precision:0.7401, Recall:0.7170, F-Measure:0.7204, Accuracy:0.7449
2016-04-13 21:00:27: Bus Stops:5140 Traffic Stops:1848
2016-04-13 21:00:27: Route ID:76, Precision:0.6476, Recall:0.6191, F-Measure:0.6270, Accuracy:0.7109
2016-04-13 21:00:27: Bus Stops:3176 Traffic Stops:1099
2016-04-13 21:00:27: Route ID:46, Precision:0.6379, Recall:0.6439, F-Measure:0.6343, Accuracy:0.7618
2016-04-13 21:00:27: Bus Stops:16044 Traffic Stops:9604
2016-04-13 21:00:27: Overall Results
2016-04-13 21:00:27: Precision:0.6687, Recall:0.6411, F-Measure:0.6412, Accuracy:0.7048
2016-04-13 21:00:27: Passenger Results
2016-04-13 21:00:27: Route ID:116, Precision:0.1095, Recall:0.1133, F-Measure:0.1098, Accuracy:0.7129
2016-04-13 21:00:27: Route ID:118, Precision:0.2952, Recall:0.3095, F-Measure:0.2993, Accuracy:0.8929
2016-04-13 21:00:27: Route ID:747, Precision:0.5574, Recall:0.5523, F-Measure:0.5381, Accuracy:0.9617
2016-04-13 21:00:27: Route ID:140, Precision:0.7699, Recall:0.6981, F-Measure:0.7081, Accuracy:0.8667
2016-04-13 21:00:27: Route ID:76, Precision:0.7842, Recall:0.7074, F-Measure:0.7194, Accuracy:0.9200
2016-04-13 21:00:27: Route ID:46, Precision:0.6118, Recall:0.5648, F-Measure:0.5678, Accuracy:0.8749
2016-04-13 21:00:27: Overall Results
2016-04-13 21:00:27: Precision:0.5213, Recall:0.4909, F-Measure:0.4904, Accuracy:0.8715
2016-04-13 21:00:27: 277 min 00 sec 611 ms
2016-04-13 21:00:27: Cleaning Results...
2016-04-13 21:00:27: 00 min 00 sec 347 ms
2016-04-13 21:00:27: Saving results as <Results>...
2016-04-13 21:00:27: 00 min 00 sec 47 ms
