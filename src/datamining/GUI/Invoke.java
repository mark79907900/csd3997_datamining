/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.GUI;

import datamining.Algorithms.KNN.KNN;
import datamining.Algorithms.ParzenWindow;
import datamining.Files.AuditLog;
import datamining.Files.Files;
import datamining.Types.BusStopCoordinates;
import datamining.Types.GPSRoutes;
import datamining.Types.Ouput.ProcessOutput;
import datamining.Types.RoutePassengers;
import datamining.Types.RouteSchedule;
import datamining.Utils.ErrorHandling;
import datamining.Utils.Profiler;
import java.util.HashMap;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Invoke {
    public static void execute(
            String fileName,
            String gpsDatasetPath,
            String passengerDatasetPath,
            String routesDatasetPath,
            String stopsDatasetPath,
            AlgorithmEnum selectedAlgorithm,
            int busStopDistance,
            int trafficDistance,
            int folds,
            int k
    ){
        try {
            // <editor-fold desc=" Create new folder and set file name ">
            String outputFileName = "";
            
            //Make folder
            String folderName = Files.makeDirectory(fileName);
            
            //check that the folder was created successfully
            if(folderName != null){
                outputFileName = String.format("%s\\%s", folderName, fileName);
            }
            
            AuditLog.setFileName(outputFileName);
            // </editor-fold>
            
            Profiler profiler = new Profiler(false);
            
            // <editor-fold desc=" Input Configuration ">
            AuditLog.results(
                    String.format("Algorithm Selected: %s",
                            selectedAlgorithm.name()),
                    true
            );
            
            AuditLog.results(
                    String.format("GPS Dataset: %s",
                            getFileNameFromPath(gpsDatasetPath)),
                    true
            );
            
            AuditLog.results(
                    String.format("Passengers Dataset: %s",
                            getFileNameFromPath(passengerDatasetPath)),
                    true
            );
            
            AuditLog.results(
                    String.format("Route Dataset: %s",
                            getFileNameFromPath(routesDatasetPath)),
                    true
            );
            
            AuditLog.results(
                    String.format("Stops Dataset: %s\n",
                            getFileNameFromPath(stopsDatasetPath)),
                    true
            );
            
            AuditLog.results(
                    String.format("Bus Stops Distance Threshold(m): %d",
                            busStopDistance),
                    true
            );
            
            AuditLog.results(
                    String.format("Traffic Stops Distance Threshold(m): %d\n",
                            trafficDistance),
                    true
            );
            
            if(selectedAlgorithm == AlgorithmEnum.KNN_WITH_PW){
                AuditLog.results(
                        String.format("Number Of Folds: %d",
                                folds),
                        true
                );
                
                AuditLog.results(
                        String.format("K: %d\n",
                                k),
                        true
                );
            }
            // </editor-fold>
            
            // <editor-fold desc=" Load DataSets ">
            AuditLog.results("Loading...", true);
            profiler.reset();
            
            GPSRoutes GPSDataset = Files.readGPSDataset(gpsDatasetPath);
            HashMap<Integer, BusStopCoordinates> busStopCoordinates = Files.readBusStopCoordinates(stopsDatasetPath);
            //KMeans kmeans = new KMeans(GPSDataset, busStopCoordinates);
            HashMap<Integer, RouteSchedule> schedule = Files.readSchedule(routesDatasetPath, busStopCoordinates);
            HashMap<Integer, RoutePassengers> passengerDataset = Files.readPassengerDataset(passengerDatasetPath);
            
            profiler.printTimeTaken();
            // </editor-fold>
            
            // <editor-fold desc=" Process Algorithm ">
            AuditLog.results("\nProcessing...", true);
            profiler.reset();
            
            switch(selectedAlgorithm){
                case PARSON_WINDOWS:{
                    new ParzenWindow(GPSDataset, schedule, passengerDataset, busStopDistance, trafficDistance);
                    break;
                }
                case KNN_WITH_PW:{
                    new KNN(GPSDataset, schedule, passengerDataset, k, folds, busStopDistance, trafficDistance);
                    break;
                }
                default:{
                    ErrorHandling.throwException("Algorithm Not Supported");
                    break;
                }
            }
            profiler.printTimeTaken();
            // </editor-fold>
            
            // <editor-fold desc=" Cleaning ">
            AuditLog.results("\nCleaning Results...", true);
            profiler.reset();
            ProcessOutput po = new ProcessOutput(GPSDataset, passengerDataset, schedule);
            profiler.printTimeTaken();
            // </editor-fold>
            
            // <editor-fold desc=" Saving ">
            AuditLog.results(String.format("\nSaving results as <%s>...", outputFileName.split("\\\\")[2]), true);
            profiler.reset();
            Files.dumpStopsDataOutput(outputFileName, po.getRouteOutputList());
            Files.dumpDirectRouteDataOutput(outputFileName, po.getLinkedTripsOutput());
            profiler.printTimeTaken();
            // </editor-fold>
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    // <editor-fold desc=" Helper Functionality ">
    /**
     *  get the file name from the path
     * @param path
     * @return
     */
    private static String getFileNameFromPath(String path){
        String out = "";
        try {
            String[] parts = path.split("\\\\");
            out = parts[parts.length-1];
            
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return out;
    }
    // </editor-fold>
}
