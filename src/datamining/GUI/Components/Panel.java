/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.GUI.Components;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author Mark
 */
public class Panel {
    private JPanel panel;
    public enum panelType{GridBagLayout, FlowLayout}
    
    /**
     * Create a panel
     * @param panelTitle
     * @param panelToolTip
     * @param type 
     */
    public Panel(String panelTitle, String panelToolTip, panelType type){
        if(type == panelType.GridBagLayout){
            panel = new JPanel(new GridBagLayout());
        }else if(type == panelType.FlowLayout){
            panel = new JPanel(new FlowLayout());
        }
        panel.setBorder(BorderFactory.createTitledBorder(panelTitle));
        panel.setToolTipText(panelToolTip);
    }
    
    /**
     * Get panel
     * @return 
     */
    public JPanel getPanel(){
        return this.panel;
    }
    
}
