/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.GUI.Components;

import datamining.Utils.ErrorHandling;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.logging.Level;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mark
 */
public class CheckBox {
    private final GridBagConstraints c;
    private JPanel panel;
    private JLabel label;
    private String labelText;
    private String toolTipText;
    private Font font;
    private int row;
    private JCheckBox checkBox;
    
    /**
     * Creates a row with a label and text field
     * @param panel the panel to which this field should be attached
     * @param row the row for the GridBagConstraints
     * @param labelText the label field
     * @param toolTipText the text shown in the tooltip
     * @param font the font
     */
    public CheckBox(JPanel panel, int row, String labelText, String toolTipText, Font font){
        this.labelText = labelText;
        this.toolTipText = toolTipText;
        this.panel = panel;
        this.c = new GridBagConstraints();
        this.font = font;
        this.row = row;
        
        generate();
    }
    
    /**
     * get the text field text value
     * @return
     */
    public boolean isSelected(){
        return checkBox.isSelected();
    }
    
    /**
     * get the checkbox object
     * @return 
     */
    public JCheckBox getCheckBox(){
        return this.checkBox;
    }
    
    /**
     * enable or disable this field
     * @param enabled 
     */
    public void setEnabled(boolean enabled){
        label.setEnabled(enabled);
        checkBox.setEnabled(enabled);
    }
    
    /**
     * Set this checkbox to checked
     * @param selected 
     */
    public void setSelected(boolean selected){
        checkBox.setSelected(selected);
    }
    
    /**
     * private method used to generate the label and checkbox
     */
    private void generate(){
        label = new JLabel(this.labelText);
        label.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 10));
        if(!toolTipText.isEmpty()){
            label.setToolTipText(toolTipText);
        }
        label.setFont(this.font);
        c.gridx = 0;
        c.gridy = this.row;
        c.weightx = 0.2;
        this.panel.add(label, c);
        
        checkBox = new JCheckBox();
        if(!toolTipText.isEmpty()){
            checkBox.setToolTipText(toolTipText);
        }
        c.gridx = 1;
        c.gridy = this.row;
        this.panel.add(checkBox, c);
        
        //listener which will change the mouse pointer
        checkBox.addMouseMotionListener(new MouseMotionListener(){
            @Override
            public void mouseMoved(MouseEvent  e) {
                try{
                    checkBox.setCursor(new Cursor(Cursor.HAND_CURSOR));
                }catch(Exception ex){
                    ErrorHandling.output(Level.WARNING, ex);
                }
            }
            @Override
            public void mouseDragged(MouseEvent e) {
            }
        });
    }
}

