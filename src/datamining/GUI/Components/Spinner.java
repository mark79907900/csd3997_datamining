/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.GUI.Components;

import java.awt.Font;
import java.awt.GridBagConstraints;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author Mark
 */
public class Spinner {
    private final GridBagConstraints c;
    private JPanel panel;
    private JLabel label;
    private String labelText;
    private String toolTipText;
    private Font font;
    private int row;
    private JSpinner spinnerField;
    
    // <editor-fold desc=" Spinner Attributes ">
    private final SpinnerModel spinnerModel;
    // </editor-fold>

    /**
     * Creates a row with a label and a spinner
     * @param panel the panel to which this field should be attached
     * @param row the row for the GridBagConstraints
     * @param labelText the label field
     * @param toolTipText
     * @param font the font
     * @param min
     * @param max
     * @param step
     * @param defaultValue
     */
    public Spinner(JPanel panel, int row, String labelText, String toolTipText, Font font, int min, int max, int step, int defaultValue){
        this.labelText = labelText;
        this.toolTipText = toolTipText;
        this.panel = panel;
        this.c = new GridBagConstraints();
        this.font = font;
        this.row = row;
        
        this.spinnerModel = new SpinnerNumberModel(defaultValue, min, max, step);
        
        generate();
    }
    
    /**
     * get the integer value from the spinner
     * @return
     */
    public int getValue(){
        return (int)spinnerField.getValue();
    }
    
    /**
     * Set the value of the field
     * @param value
     */
    public void setValue(int value){
        spinnerField.setValue(value);
    }
    
    /**
     * private method used to generate the label and field
     */
    private void generate(){
        label = new JLabel(this.labelText);
        label.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 10));
        if(!toolTipText.isEmpty()){
            label.setToolTipText(toolTipText);
        }
        label.setFont(this.font);
        c.gridx = 0;
        c.gridy = this.row;
        c.weightx = 0.2;
        this.panel.add(label, c);
        
        spinnerField = new JSpinner(this.spinnerModel);
        if(!toolTipText.isEmpty()){
            spinnerField.setToolTipText(toolTipText);
        }
        c.gridx = 1;
        c.gridy = this.row;
        this.panel.add(spinnerField, c);
    }
    
    /**
     * enable or disable this field
     * @param enabled
     */
    public void setEnabled(boolean enabled){
        label.setEnabled(enabled);
        spinnerField.setEnabled(enabled);
    }
}
