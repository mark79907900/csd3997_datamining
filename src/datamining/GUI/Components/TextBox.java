/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.GUI.Components;

import java.awt.Font;
import java.awt.GridBagConstraints;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Mark
 */
public class TextBox {
    private final GridBagConstraints c;
    private JPanel panel;
    private JLabel label;
    private String labelText;
    private String toolTipText;
    private Font font;
    private int row;
    private JTextField textField;
    
    /**
     * Creates a row with a label and text field
     * @param panel the panel to which this field should be attached
     * @param row the row for the GridBagConstraints
     * @param labelText the label field
     * @param font the font
     */
    public TextBox(JPanel panel, int row, String labelText, String toolTipText, Font font){
        this.labelText = labelText;
        this.toolTipText = toolTipText;
        this.panel = panel;
        this.c = new GridBagConstraints();
        this.font = font;
        this.row = row;
        
        generate();
    }
    
    /**
     * get the text field text value
     * @return
     */
    public String getText(){
        return textField.getText();
    }
    
    /**
     * Set the value of the field
     * @param value 
     */
    public void setValue(Object value){
        textField.setText(value.toString());
    }
    
    /**
     * Set the textbox to empty
     */
    public void setEmpty(){
        textField.setText("");
    }
    
    /**
     * private method used to generate the label and field
     */
    private void generate(){
        label = new JLabel(this.labelText);
        label.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 10));
        if(!toolTipText.isEmpty()){
            label.setToolTipText(toolTipText);
        }
        label.setFont(this.font);
        c.gridx = 0;
        c.gridy = this.row;
        c.weightx = 0.2;
        this.panel.add(label, c);
        
        textField = new JTextField(15);
        if(!toolTipText.isEmpty()){
            textField.setToolTipText(toolTipText);
        }
        c.gridx = 1;
        c.gridy = this.row;
        this.panel.add(textField, c);
    }
    
    /**
     * enable or disable this field
     * @param enabled 
     */
    public void setEnabled(boolean enabled){
        label.setEnabled(enabled);
        textField.setEnabled(enabled);
    }
}
