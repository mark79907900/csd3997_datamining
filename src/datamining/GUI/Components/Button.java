/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.GUI.Components;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mark
 */
public class Button {
    private final GridBagConstraints c;
    private JPanel panel;
    private JLabel label;
    private String labelText;
    private String toolTipText;
    private Font font;
    private int row;
    private JButton button;
    private String buttonText;
    private IButton iButtonCallback;
    
    /**
     * Creates a row with a label and text field
     * @param panel the panel to which this field should be attached
     * @param row the row for the GridBagConstraints
     * @param labelText the label field
     * @param font the font
     */
    public Button(IButton iButtonCallback, JPanel panel, int row, String labelText, String buttonText, String toolTipText, Font font){
        this.labelText = labelText;
        this.toolTipText = toolTipText;
        this.buttonText = buttonText;
        this.panel = panel;
        this.c = new GridBagConstraints();
        this.font = font;
        this.row = row;
        this.iButtonCallback = iButtonCallback;
        generate();
    }
    
    /**
     * get the text field text value
     * @return
     */
    public String getText(){
        return button.getText();
    }
    
    /**
     * private method used to generate the label and field
     */
    private void generate(){
        label = new JLabel(this.labelText);
        label.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 10));
        if(!toolTipText.isEmpty()){
            label.setToolTipText(toolTipText);
        }
        label.setFont(this.font);
        c.gridx = 0;
        c.gridy = this.row;
        c.weightx = 0.2;
        this.panel.add(label, c);
        
        button = new JButton(buttonText);
        if(!toolTipText.isEmpty()){
            button.setToolTipText(toolTipText);
        }
        c.gridx = 1;
        c.gridy = this.row;
        this.panel.add(button, c);
        
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                iButtonCallback.action((JButton)e.getSource());
            }
        });
    }
    
    /**
     * Set the dimension of the button
     * @param dimension
     */
    public void setButtonSize(Dimension dimension){
        this.button.setPreferredSize(dimension);
    }
    
    /**
     * Get the button object
     * @return 
     */
    public JButton getButton(){
        return button;
    }
}
