/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.GUI.Components;

import datamining.Utils.ErrorHandling;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.List;
import java.util.logging.Level;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

public class ListBox {
    private final String labelText;
    private final String toolTipText;
//    private JButton actionButton;
//    private final String actionButtonText;
//    private final String actionButtonToolTip;
    private final GridBagConstraints c;
    private final int row;
    private final JPanel panel;
    private final Font font;
    private JList listBox;
    private DefaultListModel listModel;
    private JScrollPane listScrollPane;
    
    /**
     * Create a file chooser row
     * @param panel the panel where this file choose should be inserted
     * @param row the row index where this file choose should be placed
     * @param labelText the label of the label
     * @param toolTipText the tool tip text
//     * @param actionButtonText Text on action button
//     * @param actionButtonToolTip Tool tip text for action button
     * @param font the font to be used
     */
    public ListBox(JPanel panel, int row, String labelText, String toolTipText, Font font){
        this.row = row;
        this.panel = panel;
        this.labelText = labelText;
        this.toolTipText = toolTipText;
//        this.actionButtonText = actionButtonText;
//        this.actionButtonToolTip = actionButtonToolTip;
        this.font = font;
        
        //set the default directory
        this.c = new GridBagConstraints();
        this.listModel = new DefaultListModel();
        this.listBox = new JList(listModel);
        this.listBox.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        this.listBox.setLayoutOrientation(JList.VERTICAL);
        this.listBox.setVisibleRowCount(-1);
        this.listScrollPane = new JScrollPane(this.listBox);
        this.listScrollPane.setPreferredSize(new Dimension(170, 75));
        
        generateRow();
    }
    
    public JList getTextField(){
        return this.listBox;
    }
    
    private void generateRow(){
        try{
            //add the button and text field
            JLabel label = new JLabel(labelText);
            label.setFont(font);
            if(!toolTipText.isEmpty()){
                label.setToolTipText(toolTipText);
            }
            c.gridx = 0;
            c.gridy = row;
            c.weightx = 0.2;
            panel.add(label, c);
           
            if(!toolTipText.isEmpty()){
                listBox.setToolTipText(toolTipText);
            }
            c.gridx = 1;
            c.gridy = row;
            panel.add(this.listScrollPane, c);
            
//            actionButton = new JButton(this.actionButtonText);
//            actionButton.setToolTipText(this.actionButtonToolTip);
//            c.insets = new Insets(2, 0, 2, 0);
//            c.gridx = 2;
//            c.gridy = row;
//            panel.add(actionButton, c);
            
            //listener which will change the mouse pointer
            listBox.addMouseMotionListener(new MouseMotionListener(){
                @Override
                public void mouseMoved(MouseEvent  e) {
                    try{
                        listBox.setCursor(new Cursor(Cursor.HAND_CURSOR));
                    }catch(Exception ex){
                        ErrorHandling.output(Level.WARNING, ex);
                    }
                }
                @Override
                public void mouseDragged(MouseEvent e) {
                }
            });
        }catch(Exception ex){
            ErrorHandling.output(Level.WARNING, ex);
        }
    }
    
    /**
     * Get a list of selected items
     * @return 
     */
    public List getSelected(){
        return this.listBox.getSelectedValuesList();
    }
    
    /**
     * Update the list of items
     * @param data 
     */
    public void updateList(Object[] data){
        this.listBox.setListData(data);
    }
    
    /**
     * remove all items from the listbox
     */
    public void clearList(){
        this.listModel.clear();
        this.listBox.setModel(listModel);
    }
    
    /**
     * Change the list mode of the JList, by default it is set to MULTIPLE_INTERVAL_SELECTION
     * @param model 
     */
    public void setSelectionMode(int model){
        this.listBox.setSelectionMode(model);
    }
    
    /**
     * get ListBox object
     * @return 
     */
    public JList getListBox(){
        return this.listBox;
    }
    
//    public JButton getActionButton(){
//        return this.actionButton;
//    }
}