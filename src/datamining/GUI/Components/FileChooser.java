/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.GUI.Components;

import datamining.DataMining;
import datamining.GUI.Components.MainWindow;
import datamining.Utils.Config;
import datamining.Utils.ErrorHandling;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.logging.Level;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Mark
 */
public class FileChooser {
    private String path;
    private final String labelText;
    private final JFileChooser fileChooser;
    private final JTextField textField;
    private final String toolTipText;
    private final GridBagConstraints c;
    private final int row;
    private final JPanel panel;
    private final MainWindow mw;
    private final Font font;
    private final IFileChooser iFileChooserCallback;
    private final String buttonTooltip;
    private JButton clear;
    
    /**
     * Create a file chooser row
     * @param mw the main window where this file chooser should be attached
     * @param panel the panel where this file choose should be inserted
     * @param row the row index where this file choose should be placed
     * @param labelText the label of the label
     * @param toolTipText the tool tip text
     * @param font the font to be used
     * @param buttonTooltip button tooltip
     */
    public FileChooser(IFileChooser iFileChooserCallback, MainWindow mw, JPanel panel, int row, String labelText, String toolTipText, String buttonTooltip, Font font){
        this.row = row;
        this.fileChooser = new JFileChooser();
        this.panel = panel;
        this.labelText = labelText;
        this.toolTipText = toolTipText;
        this.font = font;
        this.mw = mw;
        this.iFileChooserCallback = iFileChooserCallback;
        this.buttonTooltip = buttonTooltip;
        
        //set the default directory
        this.fileChooser.setCurrentDirectory(new File(DataMining.config.getPropertyString("fileChooserDefaultDirectory")));
        this.c = new GridBagConstraints();
        this.textField = new JTextField(15);
        this.path = null;
        
        generateRow();
    }
    
    public JTextField getTextField(){
        return this.textField;
    }
    
    public String getPath(){
        return path;
    }
    
    public JButton getClear(){
        return clear;
    }
    
    private void generateRow(){
        try{
            //add the button and text field
            JLabel label = new JLabel(labelText);
            label.setFont(font);
            if(!toolTipText.isEmpty()){
                label.setToolTipText(toolTipText);
            }
            c.gridx = 0;
            c.gridy = row;
            panel.add(label, c);
            
            textField.setEditable(false);
            textField.setBackground(Color.WHITE);
            if(!toolTipText.isEmpty()){
                textField.setToolTipText(toolTipText);
            }
            c.gridx = 1;
            c.gridy = row;
            c.weightx = 0.2;
            panel.add(textField, c);
            
            clear = new JButton("X");
            clear.setToolTipText(buttonTooltip);
            c.insets = new Insets(2, 0, 2, 0);
            c.gridx = 2;
            c.gridy = row;
            panel.add(clear, c);
            
            //attach an action listener to the button
            clear.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try{
                        path = null;
                        textField.setText("");
                        iFileChooserCallback.removeAndResetDataSet(e);
                    }catch(Exception ex){
                        ErrorHandling.output(Level.WARNING, ex);
                    }
                }
            });
            
            //listner which will removeAndResetDataSet the item in the field when clicked
            textField.addMouseListener(new MouseAdapter(){
                @Override
                public void mouseClicked(MouseEvent  e) {
                    try{
                        //check for a valid selection
                        if (fileChooser.showOpenDialog(mw) == JFileChooser.APPROVE_OPTION) {
                            //get the file path
                            path = fileChooser.getSelectedFile().getAbsolutePath();
                            String[] bits = path.split("\\\\");
                            String lastOne = bits[bits.length-1];
                            textField.setText(lastOne);
                        }
                        
                    }catch(Exception ex){
                        ErrorHandling.output(Level.WARNING, ex);
                    }
                }
            });
            
            //listener which will change the mouse pointer
            textField.addMouseMotionListener(new MouseMotionListener(){
                @Override
                public void mouseMoved(MouseEvent  e) {
                    try{
                        textField.setCursor(new Cursor(Cursor.HAND_CURSOR));
                    }catch(Exception ex){
                        ErrorHandling.output(Level.WARNING, ex);
                    }
                }
                @Override
                public void mouseDragged(MouseEvent e) {
                }
            });
        }catch(Exception ex){
            ErrorHandling.output(Level.WARNING, ex);
        }
        
    }
}
