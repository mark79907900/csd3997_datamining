package datamining.GUI.Components;

import datamining.DataMining;
import datamining.Utils.Config;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 * This Class produces a the main window which is a JFrame.
 * DO NOT FORGET TO SETVISIBLE THE OBJECT CREATED BY THIS CLASS AFTER ALL OF THE COMPONENTS HAVE BEEN ADDED
 * @author Mark
 */
public class MainWindow extends JFrame{
    private static MainWindow instance = null;
    
    /**
     * Create the new frame by passing the name to the JFrame parent class
     * @param WindowName
     */
    private MainWindow(String WindowName) {
        super(WindowName);
        // Sets the layout and formating for the main frame
        setLayout(new BorderLayout());
    }
    
    /**
     * Init and return the main window
     * @param WindowName
     * @return
     */
    public static MainWindow getMainWindow(String WindowName){
        if(instance == null){
            instance = new MainWindow(WindowName);
            instance.init();
        }
        return instance;
    }
    public static MainWindow getMainWindow(){
        return instance;
    }
    
    /**
     * Apply some transformation to the main frame
     */
    public void init(){
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        
        //set the defulat minimum window size
        setSize(
                DataMining.gui.getPropertyInt("mainWindowWidth"),
                DataMining.gui.getPropertyInt("mainWindowHeight")
        );
        
        //center of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }
    
    /***
     * Set the Frame to visible
     */
    public void setVisible(){
        setVisible(true);
    }
}
