package datamining.GUI;

import datamining.DataMining;
import datamining.Files.Files;
import datamining.GUI.Components.Button;
import datamining.GUI.Components.CheckBox;
import datamining.GUI.Components.ComboBox;
import datamining.GUI.Components.FileChooser;
import datamining.GUI.Components.IButton;
import datamining.GUI.Components.IComboBox;
import datamining.GUI.Components.IFileChooser;
import datamining.GUI.Components.ListBox;
import datamining.GUI.Components.MainWindow;
import datamining.GUI.Components.Panel;
import datamining.GUI.Components.TextBox;
import datamining.GUI.Components.Spinner;
import datamining.Utils.ErrorHandling;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Mark
 */
public class MainGui implements IFileChooser, IComboBox{
    private static MainGui self = null;
    
    // <editor-fold desc=" Main Window & Panels ">
    private final MainWindow mw;
    private JPanel dataSetPanel;
    private JPanel configPanel;
    private JPanel buttonsPannel;
    // </editor-fold>
    
    private Font smallFont;
    
    // <editor-fold desc=" Fields ">
    private ComboBox algorithmSelectionComboBox;
    private FileChooser gpsDatasetFileChooser;
    private FileChooser passengerDatasetFileChooser;
    // </editor-fold>
    private FileChooser stopsDatasetFileChooser;
    private FileChooser routeDatasetFileChooser;
    private TextBox outputName;
    private Spinner busStopDistanceSpinner;
    private Spinner trafficStopDistanceSpinner;
    private Spinner foldsSpinner;
    private Spinner kSpinner;
    
    /**
     * private Constructor to build the GUI
     * It creates a new Window and sets up the GUI components by invoking the private method setup
     * @param callBack
     */
    private MainGui() {
        this.mw = MainWindow.getMainWindow(DataMining.gui.getPropertyString("mainWindowName"));
        this.smallFont = new Font("Serif", Font.PLAIN, 14);
        setup();
    }
    
    /**
     * This method is used to init the GUI and ensure that only one instance can be created (Singleton)
     * @param callBack
     */
    public static void init(){
        if (self == null){
            self = new MainGui();
        }
    }
    
    /**
     * Creates the Components
     * @param callBack
     */
    private void setup(){
        try{
            // <editor-fold desc=" Panels ">
            this.dataSetPanel = new Panel(
                    DataMining.lang.getPropertyString("dataSetTitle"),
                    DataMining.lang.getPropertyHTML("dataSetToolTip"),
                    Panel.panelType.GridBagLayout
            ).getPanel();
            
            this.mw.add(this.dataSetPanel, BorderLayout.NORTH);
            
            this.configPanel = new Panel(
                    DataMining.lang.getPropertyString("configPanelTitle"),
                    DataMining.lang.getPropertyString("configPanelToolTip"),
                    Panel.panelType.GridBagLayout
            ).getPanel();
            
            this.mw.add(this.configPanel, BorderLayout.CENTER);
            
            this.buttonsPannel = new Panel(
                    "",
                    "",
                    Panel.panelType.FlowLayout
            ).getPanel();
            this.mw.add(this.buttonsPannel, BorderLayout.SOUTH);
            // </editor-fold>
            
            // <editor-fold desc=" Dataset Panel ">
            this.algorithmSelectionComboBox = new ComboBox(this, dataSetPanel, 0,
                    DataMining.lang.getPropertyString("algorithmSelectionLabel"),
                    DataMining.lang.getPropertyHTML("algorithmSelectionToolTip"),
                    smallFont
            );
            this.algorithmSelectionComboBox.updateList(AlgorithmEnum.getNames());
            
            this.outputName = new TextBox(dataSetPanel, 1,
                    DataMining.lang.getPropertyString("outputNameLabel"),
                    DataMining.lang.getPropertyString("outputNameToolTip"),
                    smallFont
            );
            
            this.gpsDatasetFileChooser = new FileChooser(this, mw, dataSetPanel, 2,
                    DataMining.lang.getPropertyString("gpsDatasetFileChooserLabel"),
                    DataMining.lang.getPropertyHTML("gpsDatasetFileChooserToolTip"),
                    DataMining.lang.getPropertyHTML("gpsDatasetFileChooserButtonToolTip"),
                    smallFont
            );
            
            this.passengerDatasetFileChooser = new FileChooser(this, mw, dataSetPanel, 3,
                    DataMining.lang.getPropertyString("passengerDatasetFileChooserLabel"),
                    DataMining.lang.getPropertyHTML("passengerDatasetFileChooserToolTip"),
                    DataMining.lang.getPropertyHTML("passengerDatasetFileChooserButtonToolTip"),
                    smallFont
            );
            
            this.routeDatasetFileChooser = new FileChooser(this, mw, dataSetPanel, 4,
                    DataMining.lang.getPropertyString("routeDatasetFileChooserLabel"),
                    DataMining.lang.getPropertyHTML("routeDatasetFileChooserToolTip"),
                    DataMining.lang.getPropertyHTML("routeDatasetFileChooserButtonToolTip"),
                    smallFont
            );
            
            this.stopsDatasetFileChooser = new FileChooser(this, mw, dataSetPanel, 5,
                    DataMining.lang.getPropertyString("stopsDatasetFileChooserLabel"),
                    DataMining.lang.getPropertyHTML("stopsDatasetFileChooserToolTip"),
                    DataMining.lang.getPropertyHTML("stopsDatasetFileChooserButtonToolTip"),
                    smallFont
            );
            // </editor-fold>
            
            // <editor-fold desc=" Config Panel ">
            this.busStopDistanceSpinner = new Spinner(this.configPanel, 0,
                    DataMining.lang.getPropertyString("busStopDistanceSpinnerLabel"),
                    DataMining.lang.getPropertyHTML("busStopDistanceSpinnerToolTip"),
                    smallFont,
                    spinnerMin("busStopMinDistance"),
                    spinnerMax("busStopMaxDistance"),
                    1,
                    spinnerMin("busStopDefaultDistance")
            );
            
            this.trafficStopDistanceSpinner = new Spinner(this.configPanel, 1,
                    DataMining.lang.getPropertyString("trafficStopDistanceSpinnerLabel"),
                    DataMining.lang.getPropertyHTML("trafficStopDistanceSpinnerToolTip"),
                    smallFont,
                    spinnerMin("trafficStopMinDistance"),
                    spinnerMax("trafficStopMaxDistance"),
                    1,
                    spinnerMin("trafficStopDefaultDistance")
            );
            
            this.foldsSpinner = new Spinner(this.configPanel, 2,
                    DataMining.lang.getPropertyString("foldsSpinnerLabel"),
                    DataMining.lang.getPropertyHTML("foldsSpinnerToolTip"),
                    smallFont,
                    spinnerMin("foldsMin"),
                    spinnerMax("foldsMax"),
                    1,
                    spinnerMin("foldsDefualt")
            );
            this.foldsSpinner.setEnabled(false);
            
            this.kSpinner = new Spinner(this.configPanel, 3,
                    DataMining.lang.getPropertyString("kSpinnerLabel"),
                    DataMining.lang.getPropertyHTML("kSpinnerToolTip"),
                    smallFont,
                    spinnerMin("kMin"),
                    spinnerMax("kMax"),
                    1,
                    spinnerMin("kDefualt")
            );
            this.kSpinner.setEnabled(false);
            
            // </editor-fold>
            
            // <editor-fold desc=" Buttons Panel ">
            //close button
            JButton close = new JButton(DataMining.lang.getPropertyString("closeButtonLabel"));
            close.setToolTipText(DataMining.lang.getPropertyString("closeButtonToolTip"));
            close.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        mw.dispose();
                    } catch (Exception ex) {
                        ErrorHandling.output(Level.SEVERE, ex);
                    }
                }
            });
            this.buttonsPannel.add(close);
            
            //load button
            JButton process = new JButton(DataMining.lang.getPropertyString("processButtonLabel"));
            process.setToolTipText(DataMining.lang.getPropertyString("processButtonToolTip"));
            process.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        if(
                                outputNameValidation() &&
                                datasetsValidation() &&
                                distanceValidation(
                                        busStopDistanceSpinner.getValue(),
                                        trafficStopDistanceSpinner.getValue()
                                )){
                            String fileName = outputName.getText();
                            String gpsPath = gpsDatasetFileChooser.getPath();
                            String passengersPath = passengerDatasetFileChooser.getPath();
                            String routePath = routeDatasetFileChooser.getPath();
                            String stopsPath = stopsDatasetFileChooser.getPath();
                            
                            AlgorithmEnum selectedAlgorithm = AlgorithmEnum.valueOf(
                                    (String)algorithmSelectionComboBox.getSelected()
                            );
                            
                            int busStopDistance = busStopDistanceSpinner.getValue();
                            int trafficStopDistance = trafficStopDistanceSpinner.getValue();
                            int folds = foldsSpinner.getValue();
                            int k = kSpinner.getValue();
                            
                            Invoke.execute(
                                    fileName, 
                                    gpsPath, 
                                    passengersPath, 
                                    routePath, 
                                    stopsPath, 
                                    selectedAlgorithm, 
                                    busStopDistance, 
                                    trafficStopDistance, 
                                    folds, 
                                    k
                            );
                        }
                    } catch (Exception ex) {
                        ErrorHandling.output(Level.SEVERE, ex);
                    }
                }
            });
            this.buttonsPannel.add(process);
            // </editor-fold>
            
            
            //set the window visiable
            this.mw.setVisible();
            this.mw.setResizable(false);
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    // <editor-fold desc=" Helper Functionality ">
    /**
     * Get the max value from a spinner property
     * @param property
     * @return
     */
    private int spinnerMax(String property){
        int spinnerMax = DataMining.gui.getPropertyInt(property);
        if(spinnerMax < 0){
            spinnerMax = Integer.MAX_VALUE;
        }
        return spinnerMax;
    }
    
    /**
     * Get the min value from a spinner property
     * @param property
     * @return
     */
    private int spinnerMin(String property){
        int spinnerMin = DataMining.gui.getPropertyInt(property);
        if(spinnerMin < 0){
            spinnerMin = 1;
        }
        return spinnerMin;
    }
    
    /**
     * Validation check for dataset selection
     * @return
     */
    private boolean datasetsValidation(){
        boolean valid = true;
        if(gpsDatasetFileChooser.getPath() == null &&
                passengerDatasetFileChooser.getPath() == null &&
                routeDatasetFileChooser.getPath() == null &&
                stopsDatasetFileChooser.getPath() == null
                ){
            errorDialog(
                    DataMining.lang.getPropertyHTML("allSetsNotNullErrorMessage")
            );
            valid = false;
        }
        return valid;
    }
    
    /**
     * Validation method used to check that the bus stop distance threshold is
     * smaller then the traffic distance threshold
     * @param busStopDistance
     * @param trafficDistance
     * @return
     */
    private boolean distanceValidation(int busStopDistance, int trafficDistance){
        if(busStopDistance >= trafficDistance){
            errorDialog(
                    DataMining.lang.getPropertyHTML("distanceErrorMessage")
            );
            return false;
        }
        return true;
    }
    
    private boolean outputNameValidation(){
        if(outputName.getText().isEmpty()){
            errorDialog(
                    DataMining.lang.getPropertyHTML("outputErrorMessage")
            );
            return false;
        }
        return true;
    }
    
    /**
     * Display a dialog box with the error message passed
     * @param errorMessage
     */
    private void errorDialog(String errorMessage){
        JOptionPane.showMessageDialog(mw, errorMessage,
                "Inane error",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    // </editor-fold>
    
    
    // <editor-fold desc=" Interfaces Impelementation ">
    @Override
    public void removeAndResetDataSet(ActionEvent e) {
    }
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        try {
            AlgorithmEnum selectedAlgorithm = AlgorithmEnum.valueOf((String)e.getItem());
            
            //enable/disable gui components based on the selected algorithm
            switch(selectedAlgorithm){
                case PARSON_WINDOWS:{
                    this.kSpinner.setEnabled(false);
                    this.foldsSpinner.setEnabled(false);
                    break;
                }
                case KNN_WITH_PW:{
                    this.kSpinner.setEnabled(true);
                    this.foldsSpinner.setEnabled(true);
                    break;
                }
                default:{
                    ErrorHandling.throwException(
                            String.format("Unsupported Algorithm %s", selectedAlgorithm.name())
                    );
                    break;
                }
            }
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    // </editor-fold>
}
