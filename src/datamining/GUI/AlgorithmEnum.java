package datamining.GUI;

import java.util.stream.Stream;

/**
 * The list of available algorithms
 * @author Mark
 */
public enum AlgorithmEnum {
    PARSON_WINDOWS,
    KNN_WITH_PW;
    
    /**
     * Returns the names of all the types of this enum
     * Implementation could be found on
     * //http://stackoverflow.com/questions/13783295/getting-all-names-in-an-enum-as-a-string
     * @return
     */
    public static String[] getNames(){
        return Stream.of(AlgorithmEnum.values()).map(AlgorithmEnum::name).toArray(String[]::new);
    }
}
