/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Holds the list of points per route
 * @author Mark
 */
public class GPSRoute {
    private ArrayList<GPSRow> GPSRowList;
    //private Set<Integer> busStopsList;
    private int routeID;
    
    /**
     * Create a new route object with the given lineID
     * @param lineID is the route id
     */
    public GPSRoute(int lineID) {
        this.GPSRowList = new ArrayList<GPSRow>();
        //this.busStopsList = new HashSet<Integer>();
        this.routeID = lineID;
    }
    
    /**
     * add a new row of data to the route
     * @param row
     */
    public void addRow(GPSRow row){
        this.GPSRowList.add(row);
        //this.busStopsList.add(row.getStopID());
    }
    
    /**
     * get the route id of this object
     * @return
     */
    public int getRouteID() {
        return this.routeID;
    }
    
    /**
     * get the list of gps points in this route
     * @return 
     */
    public ArrayList<GPSRow> getGPSRowList(){
        return this.GPSRowList;
    }
    
//    /**
//     * Get a sorted list of bus stops in this route
//     * @return
//     */
//    public ArrayList<Integer> getBusStopList(){
//        
//        /*
//        collections.sort does not work with a null value.The set may contain a
//        null value and therefore it is removed from the set and flagged hasNull
//        in order to added back to the list once the sorting is finished
//        */
//        boolean hasNull = false;
//        if(this.busStopsList.remove(null)){
//            hasNull = true;
//        }
//        
//        ArrayList<Integer> sortedList = new ArrayList<Integer>(this.busStopsList);
//        Collections.sort(sortedList);
//        
//        if(hasNull){
//            sortedList.add(null);
//        }
//        return sortedList;
//    }
//    
//    /**
//     * Get a sorted Object Array of bus stops in this route
//     * @return
//     */
//    public Object[] getBusStopObjectArray(){
//        return getBusStopList().toArray();
//    }
}
