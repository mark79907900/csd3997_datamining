/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types;

import datamining.DataMining;
import datamining.Types.BusStopCoordinates;
import datamining.Types.Passenger;

/**
 *
 * @author Mark
 */
public class LinkedTrips {
    private Passenger firstStop;
    private Passenger secondStop;
    private final String tripBusStopKey;
    private long timeStamp;
    
    public LinkedTrips(Passenger firstStop, Passenger secondStop) {
        this.firstStop = firstStop;
        this.secondStop = secondStop;
        this.tripBusStopKey = firstStop.getPredictedStopId()+ "_" + secondStop.getPredictedStopId();
        
        long secondBusTripOuputLevelOfGranularity = DataMining.config.getPropertyLong("secondBusTripOuput_LevelOfGranularity");
        this.timeStamp = firstStop.getTimeStamp() - (firstStop.getTimeStamp() % secondBusTripOuputLevelOfGranularity);
    }
    
    // <editor-fold desc=" Getters ">
    public int getfirstRouteId(){
        return firstStop.getRouteId();
    }
    
    public int getsecondRouteId(){
        return secondStop.getRouteId();
    }
    
    public long getTimeStamp() {
        return timeStamp;
    }
    
    public String getTripBusStopKey() {
        return tripBusStopKey;
    }
    
    public String getFirstStopData(){
        return  getSecondBusStopCoordinates().getStopId() + "," +
                getFirstBusStopCoordinates().getStopId() + "," +
                getFirstBusStopCoordinates().getLat()+ "," +
                getFirstBusStopCoordinates().getLon() + "\n";
    }
    
    public static String getFirstStopHeader(){
        return "SecondStopId,FirstStopId,Lat,Lon\n";
    }
    
    public String getSecondStopData(){
        return  getFirstBusStopCoordinates().getStopId() + "," +
                getSecondBusStopCoordinates().getStopId() + "," +
                getSecondBusStopCoordinates().getLat()+ "," +
                getSecondBusStopCoordinates().getLon() + "\n";
    }
    
    public static String getSecondStopHeader(){
        return "FirstStopId,SecondStopId,StopId,Lat,Lon\n";
    }
    // </editor-fold>
    
    // <editor-fold desc=" Helper Functionality ">
    private BusStopCoordinates getFirstBusStopCoordinates(){
        return firstStop.getPredictedBusStopCoordinates();
    }
    
    private BusStopCoordinates getSecondBusStopCoordinates(){
        return secondStop.getPredictedBusStopCoordinates();
    }
    // </editor-fold>
}
