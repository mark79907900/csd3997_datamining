/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Results;

import datamining.Files.AuditLog;
import datamining.Types.RouteSchedule;
import datamining.Utils.ErrorHandling;
import datamining.Utils.Profiler;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Results {
    
    //<route id, Result>
    private final HashMap<Integer, Result> routeResults;
    
    private final boolean showStopCount;
    
    public Results(boolean showStopCount) {
        this.routeResults = new HashMap<Integer, Result>();
        this.showStopCount = showStopCount;
    }
    
    /**
     * add a new route result
     * @param result
     */
    public void addRouteResults(Result result){
        routeResults.put(result.getRouteId(), result);
    }
    
    /**
     * get the result associated with the route in the routeSchedule passed.
     * If the result does not exist a new one will be created
     * @param routeSchedule
     * @return
     */
    public Result getResult(RouteSchedule routeSchedule){
        Result result = null;
        try {
            if(routeResults.containsKey(routeSchedule.getRouteId())){
                result = routeResults.get(routeSchedule.getRouteId());
            }else{
                result = new Result(routeSchedule, showStopCount);
                routeResults.put(result.getRouteId(), result);
            }
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return result;
    }
    
    /**
     * print the results
     * @param printStops
     * @param printRoute
     */
    public void print(boolean printStops, boolean printRoute){
        
        double totalPrecision = 0;
        double totalRecall = 0;
        double totalFMeasure = 0;
        double totalAccuracy = 0;
        
        for(Entry<Integer, Result> resultEntry: routeResults.entrySet()){
            Result result = resultEntry.getValue();
            //process the results
            result.print(printStops, printRoute);
            
            totalPrecision += result.getAvgPrecision();
            totalRecall += result.getAvgRecall();
            totalFMeasure += result.getAvgFMeasure();
            totalAccuracy += result.getAccuracy();
        }
        
        double avgPrecision = (totalPrecision*1.0)/routeResults.size();
        double avgRecall = (totalRecall*1.0)/routeResults.size();
        double avgFMeasure = (totalFMeasure*1.0)/routeResults.size();
        double avgAccuracy = (totalAccuracy*1.0)/routeResults.size();
        
        String overall =
                String.format(
                        "Precision:%.4f, Recall:%.4f, F-Measure:%.4f, Accuracy:%.4f", 
                        avgPrecision, 
                        avgRecall, 
                        avgFMeasure, 
                        avgAccuracy
                );
        try {
            AuditLog.results("Overall Results", true);
            AuditLog.results(overall, true);
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
}
