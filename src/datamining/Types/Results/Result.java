/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Results;

import datamining.Files.AuditLog;
import datamining.Types.BusStopCoordinates;
import datamining.Types.RouteSchedule;
import datamining.Utils.ErrorHandling;
import datamining.Utils.Profiler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Result {
    //<route id, frequency>
    private final HashMap<Integer, Integer> truePositive;
//    private final HashMap<Integer, Integer> trueNegative;
    private final HashMap<Integer, Integer> falsePositve;
    private final HashMap<Integer, Integer> falseNegative;
    
    private final HashMap<Integer, Double> precision;
    private final HashMap<Integer, Double> recall;
    private final HashMap<Integer, Double> fMeasure;
    
    private int total;
    private int correct;
    private double accuracy;
    
    private double avgPrecision;
    private double avgRecall;
    private double avgFMeasure;
    
    private final int routeId;
    
    private int busStopCounter;
    private int trafficCounter;
    
    private boolean showStopCount;
    
    public Result(RouteSchedule routeSchedule, boolean showStopCount) {
        this.truePositive = new HashMap<Integer, Integer>();
//        this.trueNegative = new HashMap<Integer, Integer>();
        this.falsePositve = new HashMap<Integer, Integer>();
        this.falseNegative = new HashMap<Integer, Integer>();
        
        for(Entry<Integer, BusStopCoordinates> busStopEntry :routeSchedule.getBusStopsMap().entrySet()){
            int busStopId = busStopEntry.getKey();
            this.truePositive.put(busStopId, 0);
//            this.trueNegative.put(busStopId, 0);
            this.falsePositve.put(busStopId, 0);
            this.falseNegative.put(busStopId, 0);
        }
        
        this.precision = new HashMap<Integer, Double>();
        this.recall = new HashMap<Integer, Double>();
        this.fMeasure = new HashMap<Integer, Double>();
        this.total = 0;
        this.correct = 0;
        this.avgPrecision = 0;
        this.avgRecall = 0;
        this.avgFMeasure = 0;
        this.routeId = routeSchedule.getRouteId();
        
        this.busStopCounter = 0;
        this.showStopCount = showStopCount;
    }
    
    public Result(int routeId, HashMap<Integer, BusStopCoordinates> busStopCoordinates) {
        this.truePositive = new HashMap<Integer, Integer>();
//        this.trueNegative = new HashMap<Integer, Integer>();
        this.falsePositve = new HashMap<Integer, Integer>();
        this.falseNegative = new HashMap<Integer, Integer>();
        
        for(Entry<Integer, BusStopCoordinates> busStopEntry :busStopCoordinates.entrySet()){
            int busStopId = busStopEntry.getKey();
            this.truePositive.put(busStopId, 0);
//            this.trueNegative.put(busStopId, 0);
            this.falsePositve.put(busStopId, 0);
            this.falseNegative.put(busStopId, 0);
        }
        
        this.precision = new HashMap<Integer, Double>();
        this.recall = new HashMap<Integer, Double>();
        this.fMeasure = new HashMap<Integer, Double>();
        this.total = 0;
        this.correct = 0;
        this.avgPrecision = 0;
        this.avgRecall = 0;
        this.avgFMeasure = 0;
        this.routeId = routeId;
        
        this.busStopCounter = 0;
    }
    
    /**
     * Add a new test row
     * @param actualStopId
     * @param predictedStopId
     */
    public void addClassification(int actualStopId, int predictedStopId){
        try {
            
            //ensure that the sets are of the same size
            if(this.truePositive.size() == this.falseNegative.size() && this.falseNegative.size() == this.falsePositve.size()){
                
                //loop for the number of
                for(Entry<Integer, Integer> truePositiveEntry :this.truePositive.entrySet()){
                    int stopId = truePositiveEntry.getKey();
                    
                    // <editor-fold desc=" true positive ">
                    if(stopId == actualStopId && stopId == predictedStopId){
                        truePositiveEntry.setValue(truePositiveEntry.getValue() + 1);
                    }
                    // </editor-fold>
                    
                    // <editor-fold desc=" true negative ">
//                    if(stopId != actualStopId && stopId != predictedStopId){
//                        trueNegative.put(stopId, trueNegative.get(stopId) + 1);
//                    }
                    // </editor-fold>
                    
                    // <editor-fold desc=" false negative ">
                    if(stopId == actualStopId && stopId != predictedStopId){
                        falseNegative.put(stopId, falseNegative.get(stopId) + 1);
                    }
                    // </editor-fold>
                    
                    // <editor-fold desc=" false positive ">
                    if(stopId != actualStopId && stopId == predictedStopId){
                        falsePositve.put(stopId, falsePositve.get(stopId) + 1);
                    }
                    // </editor-fold>
                }
                
            }else{
                ErrorHandling.throwException("There is an inconsistancy in the result maps, the precision, recall and f-measure will not be calculated correctly");
            }
            
            if(actualStopId == predictedStopId){
                correct++;
            }
            total++;
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * print the full results(for all routes)
     */
    public void print(boolean printStops, boolean printRoute){
        calculatePrecision();
        calculateRecall();
        calculateFMeasure();
        accuracy();
        calculateAvgs();
        
        //print stops data
        if(printStops){
            for(Entry<Integer, Integer> entry : truePositive.entrySet()){
                int routeId = entry.getKey();
                
                String output = String.format("Stop ID:%d Precision:%.4f Recall:%.4f F-Measure:%.4f",
                        routeId,
                        precision.getOrDefault(routeId, 0.0),
                        recall.getOrDefault(routeId, 0.0),
                        fMeasure.getOrDefault(routeId, 0.0)
                );
                
                System.out.println(output);
            }
        }
        
        //print route data
        if(printRoute){
            String metrics =
                    String.format(
                            "Route ID:%d, Precision:%.4f, Recall:%.4f, F-Measure:%.4f, Accuracy:%.4f",
                            routeId,
                            avgPrecision,
                            avgRecall,
                            avgFMeasure,
                            accuracy
                    );
            
            String stops =
                    String.format(
                            "Bus Stops:%d Traffic Stops:%d",
                            busStopCounter,
                            trafficCounter
                    );
            try {
                AuditLog.results(metrics, true);
                if(showStopCount){
                    AuditLog.results(stops, true);
                }
            } catch (Exception ex) {
                ErrorHandling.output(Level.SEVERE, ex);
            }
        }
        
    }
    
    public double getAccuracy() {
        return accuracy;
    }
    
    public double getAvgPrecision() {
        return avgPrecision;
    }
    
    public double getAvgRecall() {
        return avgRecall;
    }
    
    public double getAvgFMeasure() {
        return avgFMeasure;
    }
    
    public int getRouteId() {
        return routeId;
    }
    
    public int getBusStopCounter() {
        return busStopCounter;
    }
    
    public int getTrafficCounter() {
        return trafficCounter;
    }
    
    public void incrementBusStopCounter() {
        this.busStopCounter++;
    }
    
    public void incrementTrafficCounter() {
        this.trafficCounter++;
    }
    
    
    /**
     * method used to calculate the precision
     * Precision = True Positives/(True positives + False positives)
     */
    private void calculatePrecision(){
        for(Entry<Integer, Integer> entry : truePositive.entrySet()){
            int routeId = entry.getKey();
            int truePositiveCount = truePositive.getOrDefault(routeId, 0);
            int falsePositveCount = falsePositve.getOrDefault(routeId, 0);
            
            double value = (truePositiveCount*1.0)/(truePositiveCount+falsePositveCount);
            precision.put(routeId, value);
        }
    }
    
    /**
     * method used to calculate the recall
     * Recall = True Positives/Total Number = True Positive/(True positive + False Negative)
     */
    private void calculateRecall(){
        for(Entry<Integer, Integer> entry : truePositive.entrySet()){
            int routeId = entry.getKey();
            int truePositiveCount = truePositive.getOrDefault(routeId, 0);
            int falseNegativeCount = falseNegative.getOrDefault(routeId, 0);
            
            double value = (truePositiveCount*1.0)/(truePositiveCount+falseNegativeCount);
            recall.put(routeId, value);
        }
    }
    
    /**
     * method used to calculate the f-measure
     * f-measure = 2 * ((precision * recall)/(precision + recall))
     */
    private void calculateFMeasure(){
        for(Entry<Integer, Integer> entry : truePositive.entrySet()){
            int routeId = entry.getKey();
            double precisionCount = precision.getOrDefault(routeId, 0.0);
            double recallCount = recall.getOrDefault(routeId, 0.0);
            
            double value = 2 * ((precisionCount*recallCount)/(precisionCount+recallCount));
            fMeasure.put(routeId, value);
        }
    }
    
    /**
     * Method used to calculate the average precision, recall and F-Measure
     */
    private void calculateAvgs(){
        double totalPrecision = 0;
        double totalRecall = 0;
        double totalFMeasure = 0;
        
        for(Entry<Integer, Integer> entry : truePositive.entrySet()){
            int routeId = entry.getKey();
            
            if(!precision.getOrDefault(routeId, 0.0).isNaN()){
                totalPrecision += precision.getOrDefault(routeId, 0.0);
            }
            
            if(!recall.getOrDefault(routeId, 0.0).isNaN()){
                totalRecall += recall.getOrDefault(routeId, 0.0);
            }
            
            if(!fMeasure.getOrDefault(routeId, 0.0).isNaN()){
                totalFMeasure += fMeasure.getOrDefault(routeId, 0.0);
            }
        }
        
        avgPrecision = (totalPrecision*1.0)/truePositive.size();
        avgRecall = (totalRecall*1.0)/truePositive.size();
        avgFMeasure = (totalFMeasure*1.0)/truePositive.size();
    }
    
    /**
     * method used to calculate the over all accuracy
     */
    private void accuracy(){
        accuracy = (correct*1.0)/total;
    }
    
    
}
