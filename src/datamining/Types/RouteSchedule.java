/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types;

import datamining.DataMining;
import datamining.Utils.ErrorHandling;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * @author Mark
 */
public class RouteSchedule {
    private final int routeId;
    
    //contians only the bus stops and cordinates of the stops attached with this route, <Bus Stop Id, BusStopCoordinates>
    private HashMap<Integer, BusStopCoordinates> busStopsMap;
    
    //containes all the cordinates for all of the bus stops <Bus Stop Id, BusStopCoordinates>
    private final HashMap<Integer, BusStopCoordinates> busStopCoordinatesMap;
    
    /**
     * create a new route schedule with at least one stop id
     * @param busStopCoordinatesMap
     * @param stopId
     * @param routeId
     */
    public RouteSchedule(HashMap<Integer, BusStopCoordinates> busStopCoordinatesMap, int stopId, int routeId) {
        this.busStopsMap = new HashMap<Integer, BusStopCoordinates>();
        this.busStopCoordinatesMap = busStopCoordinatesMap;
        this.routeId = routeId;
        addBusStopRoute(stopId);
    }
    
    public int getRouteId() {
        return routeId;
    }
    
    public HashMap<Integer, BusStopCoordinates> getBusStopsMap() {
        return busStopsMap;
    }
    
    public BusStopCoordinates getBusStopCoordinates(int busStopId){
        return busStopCoordinatesMap.get(busStopId);
    }
    
    /**
     * add another stop id to this route schedule
     * @param busStop
     */
    public void addBusStopRoute(int busStop){
        try{
            BusStopCoordinates busStopCoordinate = busStopCoordinatesMap.get(busStop);
            
            if(busStopCoordinate != null){
                busStopsMap.put(busStop, busStopCoordinate);
            }else{
                if(DataMining.config.getPropertyBoolean("showMissingStops")){
                    ErrorHandling.throwException(String.format("There are no cordinates for bus stop with id %d in route %d", busStop, this.routeId));
                }
            }
        } catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
}
