/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types;

import datamining.DataMining;
import datamining.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class GPSRow {
    public enum RowType{UNCLASSIFIED, BUS_STOP, TRAFFIC_STOP, NOISE};
    
    // <editor-fold desc=" Loaded Attributes ">
    private final long timeStamp;
    private final String dateTime;
    private final int routeID;
    private final double lon;
    private final double lat;
    private final Integer stopID;
    // </editor-fold>
    
    // <editor-fold desc=" Predicted Attributes ">
    private BusStopCoordinates predictedBusStopCoordinates;
    private RowType rowType;
    private ArrayList<Passenger> onBoardedPassengerList;
    // </editor-fold>
    
    /**
     * create a new GPSRow object from the line.
     * This line is read from the CSV file.
     * @param lineValue
     */
    public GPSRow(String lineValue) {
        String[] parts = lineValue.split(",");
        
        try{
            
            //throw exception if the gps point passed does not have the expected number of columns
            int colCount = DataMining.config.getPropertyInt("GPSDataSet_NumberOfColumns");
            if(parts.length != colCount){
                ErrorHandling.throwException(
                        String.format("This GPS row data does not have the expected number of columns (%d), %d columns where read", colCount, parts.length)
                );
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        // <editor-fold desc=" Attribute parsing ">
        this.timeStamp = Long.parseLong(parts[0]);
        this.dateTime = parts[1];
        this.routeID = Integer.parseInt(parts[2]);
        this.lon = Double.parseDouble(parts[3]);
        this.lat = Double.parseDouble(parts[4]);
        this.stopID = Integer.parseInt(parts[5]);
        // </editor-fold>
        
        // <editor-fold desc=" Predicted init ">
        this.predictedBusStopCoordinates = null;
        this.rowType = RowType.UNCLASSIFIED;
        this.onBoardedPassengerList = new ArrayList<Passenger>();
        // </editor-fold>
    }
    
    /**
     * get the line id of this GPS row object
     * @return
     */
    public int getRouteID() {
        return routeID;
    }
    
    public Integer getStopID() {
        return stopID;
    }
    
    public double getLon() {
        return lon;
    }
    
    public double getLat() {
        return lat;
    }
    
    public long getTimeStamp() {
        return timeStamp;
    }
    
    public int getPredictedStopId() {
        if(predictedBusStopCoordinates != null){
            return predictedBusStopCoordinates.getStopId();
        }
        return -1;
    }
    
    public RowType getRowType() {
        return rowType;
    }
    
    public double getPredictedLon() {
        if(predictedBusStopCoordinates != null){
            return predictedBusStopCoordinates.getLon();
        }
        return 0.0;
    }
    
    public double getPredictedLat() {
        if(predictedBusStopCoordinates != null){
            return predictedBusStopCoordinates.getLat();
        }
        return 0.0;
    }
    
    public BusStopCoordinates getPredictedBusStopCoordinates() {
        return predictedBusStopCoordinates;
    }
    
    public void setRowType(RowType rowType) {
        this.rowType = rowType;
    }
    
    /**
     * add a new passenger to this bus stop
     * @param passenger
     */
    public void addPassenger(Passenger passenger){
        this.onBoardedPassengerList.add(passenger);
    }
    
    /**
     * Set the predicted coordinates and row type based on the bus Stop classified
     * @param predictedStopId
     * @param busStopCoordinates
     * @param rowType
     */
    public void setPredictedValues(BusStopCoordinates busStopCoordinates, RowType rowType) {
        this.predictedBusStopCoordinates = busStopCoordinates;
        this.rowType = rowType;
    }
    
    /**
     * get the number of on boarded passengers
     * @return
     */
    public int getNumberOfPassengers(){
        return this.onBoardedPassengerList.size();
    }
    
    @Override
    public String toString(){
        return "" + timeStamp + "," +
                dateTime + "," +
                routeID + "," +
                lon + "," +
                lat + "," +
                stopID + "\n";
    }
    
    public static String toStringTtiles(){
        return "TimeStamp,DateTime,Route ID,Lon,Lat,Stop ID\n";
    }
    
}
