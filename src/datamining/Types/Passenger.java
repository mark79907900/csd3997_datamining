/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types;

import datamining.DataMining;
import datamining.Utils.ErrorHandling;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Passenger {
    
    // <editor-fold desc=" Loaded attributes ">
    private final long cardNumber;
    private final long timeStamp;
    private final String dateTime;
    private final int routeId;
    private final int busStopId;
    private final double realLon;
    private final double realLat;
    // </editor-fold>
    
    // <editor-fold desc=" Predicted attributes ">
    private BusStopCoordinates predictedBusStopCoordinates;
    // </editor-fold>
    
    
    public Passenger(String lineValue) {
        String[] parts = lineValue.split(",");
        
        try{
            //throw exception if the gps point passed does not have the expected number of columns
            int colCount =  DataMining.config.getPropertyInt("passengerDataset_NumberOfColumns");
            if(parts.length != colCount){
                ErrorHandling.throwException(
                        String.format("This Passenger does not have the expected number of columns (%d), %d columns where read", colCount, parts.length)
                );
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        // <editor-fold desc=" Parse lineValue to the respective attributes ">
        this.cardNumber = Long.parseLong(parts[0]);
        this.timeStamp = Long.parseLong(parts[1]);
        this.dateTime = parts[2];
        this.routeId = Integer.parseInt(parts[3]);
        this.busStopId = Integer.parseInt(parts[4]);
        this.realLon = Double.parseDouble(parts[5]);
        this.realLat = Double.parseDouble(parts[6]);
        // </editor-fold>
        
        // <editor-fold desc=" Predicted values init ">
        this.predictedBusStopCoordinates = null;
        // </editor-fold>
    }
    
    public long getCardNumber() {
        return cardNumber;
    }
    
    public long getTimeStamp() {
        return timeStamp;
    }
    
    public int getRouteId() {
        return routeId;
    }
    
    public double getLon() {
        return realLon;
    }
    
    public double getLat() {
        return realLat;
    }
    
    public int getBusStopId() {
        return busStopId;
    }
    
    public int getPredictedStopId() {
        if(predictedBusStopCoordinates != null){
            return predictedBusStopCoordinates.getStopId();
        }
        return -1;
    }

    public BusStopCoordinates getPredictedBusStopCoordinates() {
        return predictedBusStopCoordinates;
    }
    
    /**
     * Set the predicted bus stop id and coordinates
     * @param busStopCoordinates
     */
    public void setPredictedValues(BusStopCoordinates busStopCoordinates){
        this.predictedBusStopCoordinates = busStopCoordinates;
    }
    
    @Override
    public String toString(){
        return "" + cardNumber + "," +
                timeStamp + "," +
                routeId + "," +
                busStopId + "," +
                dateTime + "," +
                routeId + "," +
                busStopId + "," +
                realLon + "," +
                realLat +  "\n";
    }
    
    public static String toStringTitles(){
        return "Card Number,TimeStamp,Route ID,Stop ID,Name,Surname,"
                + "Real Lon,Real Lat\n";
    }
}
