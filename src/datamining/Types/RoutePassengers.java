/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types;

import java.util.TreeMap;
import datamining.DataMining;

/**
 *
 * @author Mark
 */
public class RoutePassengers {
    
    //the map is used to split the passengers in bins with specific time interval
    //<timestamp, Passengers>
    private TreeMap<Long, Passengers> routePassengerMap;
    
    public RoutePassengers(Passenger passenger) {
        this.routePassengerMap = new TreeMap<Long, Passengers>();
        addPassenger(passenger);
    }
    
    /**
     * add a new passenger into the appropriate time bin.
     * If a bin does not exists, a new bin is created
     * @param passenger
     */
    public void addPassenger(Passenger passenger){
        long passengerTimeStampBinSize = 
                DataMining.config.getPropertyLong("passengerTimeStampBinSize");
        long timeStamp = passenger.getTimeStamp();
        
        // <editor-fold desc=" check if there is a time bin with lower value ">
        if (this.routePassengerMap.floorKey(timeStamp) != null) {
            long tempBin = this.routePassengerMap.floorKey(timeStamp);
            
            //if it is greater then the bin limit, create a new bin
            if ((timeStamp - tempBin) >= passengerTimeStampBinSize) {
                long newBin = 
                        (timeStamp / passengerTimeStampBinSize) 
                        * passengerTimeStampBinSize;
                this.routePassengerMap.put(newBin, new Passengers(passenger));
            } //if it is within limit get this bin
            else {
                this.routePassengerMap.get(tempBin).addPassenger(passenger);
            }
        }
        // </editor-fold>
        
        // <editor-fold desc=" else if no bin exist create one with the time stamp and passenger passed ">
        else {
            long newBin = 
                    (timeStamp / passengerTimeStampBinSize) 
                    * passengerTimeStampBinSize;
            this.routePassengerMap.put(newBin, new Passengers(passenger));
        }
        // </editor-fold>
    }
    
    // <editor-fold desc=" Getters ">
    /**
     * get the passengers which are in the same time bin of the timeStamp passed
     * @param timeStamp
     * @return
     */
    public Passengers getPassengers(long timeStamp){
        return routePassengerMap.get(routePassengerMap.floorKey(timeStamp));
    }
    
    public TreeMap<Long, Passengers> getRoutePassengerMap() {
        return routePassengerMap;
    }
    // </editor-fold>
}
