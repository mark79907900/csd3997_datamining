/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Ouput.StopsData;

import datamining.Types.Ouput.StopsData.StopsDataHourlyOutput;
import datamining.DataMining;
import datamining.Types.BusStopCoordinates;
import datamining.Types.GPSRow;
import datamining.Utils.ErrorHandling;
import java.util.TreeMap;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class StopsDataOutput {
    private final int routeID;
    private final int stopID;
    private final double lon;
    private final double lat;
    
    //<timeStamp, HourlyOutput>
    private final TreeMap<Long, StopsDataHourlyOutput> hourlyOutputMap;
    
    public StopsDataOutput(int routeID, BusStopCoordinates busStopCoordinates) {
        this.routeID = routeID;
        this.stopID = busStopCoordinates.getStopId();
        this.lon = busStopCoordinates.getLon();
        this.lat = busStopCoordinates.getLat();
        this.hourlyOutputMap = new TreeMap<Long, StopsDataHourlyOutput>();
    }
    
    public void add(long timeStamp, int numberOfPassengers, GPSRow.RowType stopType){
        try {
            long timeLevelOfGranularity = DataMining.config.getPropertyLong("outputTime_LevelOfGranularity");
            
            /*
            convert the time stamp into the level of granularity in the config file
            */
            long processedTimeStamp = timeStamp - (timeStamp % timeLevelOfGranularity);
            
            StopsDataHourlyOutput hourlyOutput = null;
            
            //if stops already exist for this time period, update
            if(hourlyOutputMap.containsKey(processedTimeStamp)){
                hourlyOutput = hourlyOutputMap.get(processedTimeStamp);
            }
            //if stops do not exist, create a new one
            else{
                hourlyOutput = new StopsDataHourlyOutput(processedTimeStamp);
                hourlyOutputMap.put(processedTimeStamp, hourlyOutput);
            }
            
            //check that the creating process has processed safely
            if(hourlyOutput != null){
                hourlyOutput.incrementPassengers(numberOfPassengers);
                if(stopType == GPSRow.RowType.BUS_STOP){
                    hourlyOutput.incrementBusStops(1);
                }else if(stopType == GPSRow.RowType.TRAFFIC_STOP){
                    hourlyOutput.incrementTrafficStops(1);
                }
            }else{
                ErrorHandling.throwException("Hourly Output was not created successfully when processing the output results");
            }
            
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }

    public TreeMap<Long, StopsDataHourlyOutput> getHourlyOutputMap() {
        return hourlyOutputMap;
    }
    
    /**
     * Get a string with the values of the object.
     * (Route ID,Stop ID,Lon,Lat)
     * @return 
     */
    @Override
    public String toString(){
        return "" +
                routeID + "," +
                stopID + "," +
                lon + "," +
                lat + ",";
    }
    
    public static String toStringTitles(){
        return "Route ID,Stop ID,Lon,Lat,";
    }
    
}
