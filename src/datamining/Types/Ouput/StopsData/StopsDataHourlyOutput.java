/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining.Types.Ouput.StopsData;

import datamining.Utils.TimeConvertor;

/**
 * @author Mark
 */
public class StopsDataHourlyOutput {
    private int numberOfOnboardedPassengers;
    private int numberOfTrafficStops;
    private int numberOfBusStops;
    private final long timeStamp;
    private final String dateTime;

    public StopsDataHourlyOutput(long timeStamp) {
        this.timeStamp = timeStamp;
        this.dateTime = TimeConvertor.toString(timeStamp);
        this.numberOfOnboardedPassengers = 0;
        this.numberOfTrafficStops = 0;
        this.numberOfBusStops = 0;
    }
    
    public void incrementPassengers(int by) {
        this.numberOfOnboardedPassengers += by;
    }
    
    public void incrementTrafficStops(int by) {
        this.numberOfTrafficStops+= by;
    }
    
    public void incrementBusStops(int by) {
        this.numberOfBusStops+= by;
    }
    
    /**
     * Get a string with the values of the object.
     * (TimeStamp,DateTime,NumOfOnboardedPassengers,NumOfTrafficStops,NumOfBusStops\n)
     * @return 
     */
    @Override
    public String toString(){
        return "" +
                timeStamp + "," +
                dateTime + "," +
                numberOfOnboardedPassengers + "," +
                numberOfTrafficStops + "," +
                numberOfBusStops + "\n";
    }
    
    public static String toStringTitles(){
        return "TimeStamp,DateTime,NumOfOnboardedPassengers,NumOfTrafficStops"
                + ",NumOfBusStops\n";
    }
}
