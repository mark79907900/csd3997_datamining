/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Ouput;

import datamining.Types.BusStopCoordinates;
import datamining.Types.GPSRoute;
import datamining.Types.GPSRoutes;
import datamining.Types.GPSRow;
import datamining.Types.LinkedTrips;
import datamining.Types.Ouput.PassengersData.LinkedTripsGroupedByTimeStamp;
import datamining.Types.Ouput.PassengersData.PassengerTripTransactions;
import datamining.Types.Ouput.StopsData.StopsDataOutput;
import datamining.Types.Passenger;
import datamining.Types.Passengers;
import datamining.Types.RoutePassengers;
import datamining.Types.RouteSchedule;
import datamining.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class ProcessOutput {
    // <editor-fold desc=" passed sets ">
    private final GPSRoutes GPSDataset;
    //<RouteId, RoutePassengers>
    private final HashMap<Integer, RoutePassengers> passengerDataset;
    //<RouteId, RouteSchedule>
    private final HashMap<Integer, RouteSchedule> schedule;
    // </editor-fold>
    
    
    // <editor-fold desc=" Output Lists/Maps ">
    //<Stop id, Output>
    private final ArrayList<HashMap<Integer, StopsDataOutput>> routeOutputList;
    //<tripBusStopKey, LinkedTripsGroupedByTimeStamp>
    private final HashMap<String, LinkedTripsGroupedByTimeStamp> LinkedTripsOutput;
    // </editor-fold>
    
    public ProcessOutput(GPSRoutes GPSDataset, HashMap<Integer, RoutePassengers> passengerDataset, HashMap<Integer, RouteSchedule> schedule) {
        this.GPSDataset = GPSDataset;
        this.schedule = schedule;
        this.passengerDataset = passengerDataset;
        this.routeOutputList = new ArrayList<HashMap<Integer, StopsDataOutput>>();
        this.LinkedTripsOutput = new HashMap<String, LinkedTripsGroupedByTimeStamp>();
        
        stopsData();
        directRouteData();
    }
    
    // <editor-fold desc=" Aggregate Functions ">
    /**
     * process the results related with the stops data
     */
    private void stopsData(){
        try {
            //loop all of the bus stops
            for(GPSRoute route:this.GPSDataset.getRouteList()){
                
                HashMap<Integer, StopsDataOutput> stopsDataOutputMap = new HashMap<Integer, StopsDataOutput>();
                this.routeOutputList.add(stopsDataOutputMap);
                
                //get the scheduled bus stops in this route
                RouteSchedule routeSchedule = schedule.get(route.getRouteID());
                
                //create an output for all of the bus stops in each route
                for(Entry<Integer, BusStopCoordinates> busStopsMap : routeSchedule.getBusStopsMap().entrySet()){
                    BusStopCoordinates busStopCoordinates = busStopsMap.getValue();
                    
                    if(busStopCoordinates != null){
                        //for each bus stop in the schedule put a stop
                        stopsDataOutputMap.put(busStopCoordinates.getStopId(), new StopsDataOutput(route.getRouteID(), busStopCoordinates));
                    }else{
                        ErrorHandling.notifyError(
                                String.format(
                                        "The Routes-Stop set does not contain bus stop with id %d for route %d",
                                        route.getRouteID(),
                                        busStopsMap.getKey()
                                        )
                        );
                    }
                }
                
                for(GPSRow gpsRow: route.getGPSRowList()){
                    //exclude an classified stops
                    if(gpsRow.getPredictedStopId() != -1 && gpsRow.getRowType() != GPSRow.RowType.NOISE){
                        StopsDataOutput stopsDataOutput = stopsDataOutputMap.get(gpsRow.getPredictedStopId());
                        
                        if(stopsDataOutput != null){
                            stopsDataOutput.add(gpsRow.getTimeStamp(), gpsRow.getNumberOfPassengers(), gpsRow.getRowType());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * process the results for the direct route prediction
     */
    private void directRouteData(){
        try {
            //<CardNumber, PassengerDataOutput>
            HashMap<Long, PassengerTripTransactions> cardNumberPassengerTransactionsMap = new HashMap<Long, PassengerTripTransactions>();
            ArrayList<LinkedTrips> linkedTripsList = new ArrayList<LinkedTrips>();
            
            //loop the routes
            for(GPSRoute route :GPSDataset.getRouteList()){
                int routeId = route.getRouteID();
                
                //get the passengers which are on this route
                RoutePassengers routePassengers = passengerDataset.get(routeId);
                
                //get the passengers in all time slots for this route
                for(Entry<Long, Passengers> passengersEntry :routePassengers.getRoutePassengerMap().entrySet()){
                    Passengers passengers = passengersEntry.getValue();
                    
                    //get all of the passengers
                    for(Passenger passenger : passengers.getPassengerList()){
                        
                        if(passenger.getPredictedStopId() != -1){
                            
                            /*
                            check if the passenger had any additional trips and is
                            already in the list
                            */
                            if(cardNumberPassengerTransactionsMap.containsKey(passenger.getCardNumber())){
                                PassengerTripTransactions passengerOutput = cardNumberPassengerTransactionsMap.get(passenger.getCardNumber());
                                passengerOutput.addPassenger(passenger);
                            }else{
                                PassengerTripTransactions passengerOutput = new PassengerTripTransactions(passenger);
                                cardNumberPassengerTransactionsMap.put(passenger.getCardNumber(), passengerOutput);
                            }
                        }
                    }
                }
            }
            
            //loop all of the passengers and find all of the linked trips
            for(Entry<Long, PassengerTripTransactions> passengerTripTransactionsEntry : cardNumberPassengerTransactionsMap.entrySet()){
                ArrayList<LinkedTrips> detectedLinkedTrips = passengerTripTransactionsEntry.getValue().detectLinkedTrips();
                linkedTripsList.addAll(detectedLinkedTrips);
            }
            
            //loop in the linkedTripsList to sort the linkedTrips accordingly
            for(LinkedTrips linkedTrips :linkedTripsList){
                final String tripBusStopKey = linkedTrips.getTripBusStopKey();
                
                //if this trip already exist increment frequency
                if(LinkedTripsOutput.containsKey(tripBusStopKey)){
                    LinkedTripsOutput.get(tripBusStopKey).add(linkedTrips);
                }
                //if this trip does not exist, create a new one
                else{
                    LinkedTripsGroupedByTimeStamp linkedTripsOutput = new LinkedTripsGroupedByTimeStamp(linkedTrips);
                    LinkedTripsOutput.put(tripBusStopKey, linkedTripsOutput);
                }
            }
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    // </editor-fold>
    
    // <editor-fold desc=" Getters ">
    public ArrayList<HashMap<Integer, StopsDataOutput>> getRouteOutputList() {
        return routeOutputList;
    }
    
    public HashMap<String, LinkedTripsGroupedByTimeStamp> getLinkedTripsOutput() {
        return LinkedTripsOutput;
    }
    // </editor-fold>
}
