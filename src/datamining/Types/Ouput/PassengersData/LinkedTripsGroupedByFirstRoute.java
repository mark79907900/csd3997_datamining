/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Ouput.PassengersData;

import datamining.Types.LinkedTrips;
import datamining.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class LinkedTripsGroupedByFirstRoute {
    //<first route id, LinkedTripsGroupedBySecondRoute>
    private final HashMap<Integer, LinkedTripsGroupedBySecondRoute> fRouteLinkedTripsMap;
    private final long timeStamp;
    private int frequencyByTimeStamp;
    
    public LinkedTripsGroupedByFirstRoute(LinkedTrips linkedTrips) {
        this.timeStamp = linkedTrips.getTimeStamp();
        this.fRouteLinkedTripsMap = new HashMap<Integer, LinkedTripsGroupedBySecondRoute>();
        this.frequencyByTimeStamp = 0;
        
        add(linkedTrips);
        
    }
    
    /**
     * add the link trips to the list
     * @param linkedTrips
     */
    public void add(LinkedTrips linkedTrips){
        if(fRouteLinkedTripsMap.containsKey(linkedTrips.getfirstRouteId())){
            fRouteLinkedTripsMap.get(linkedTrips.getfirstRouteId()).add(linkedTrips);
        }else{
            LinkedTripsGroupedBySecondRoute linkedTripsGroupedBySecondRoute = new LinkedTripsGroupedBySecondRoute(linkedTrips);
            fRouteLinkedTripsMap.put(linkedTrips.getfirstRouteId(), linkedTripsGroupedBySecondRoute);
        }
        this.frequencyByTimeStamp++;
    }
    
    // <editor-fold desc=" Getters ">
    public String getRouteData(){
        return  this.timeStamp + "," +
                this.frequencyByTimeStamp + ",";
    }
    
    public static String getRouteHeader(){
        return "TimeStamp,Frequency_ByTimeStamp,";
    }
    
    public HashMap<Integer, LinkedTripsGroupedBySecondRoute> getfRouteLinkedTripsMap() {
        return fRouteLinkedTripsMap;
    }
    // </editor-fold>
}
