/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Ouput.PassengersData;

import datamining.Types.LinkedTrips;
import java.util.ArrayList;

/**
 *
 * @author Mark
 */
public class LinkedTripsFrequency {
    private final ArrayList<LinkedTrips> linkedTripsList;
    private final int secondRouteId;
    
    private final String cachedFirstRouteData;
    private final String cachedSecondRouteData;
    
    public LinkedTripsFrequency(LinkedTrips linkedTrips) {
        this.linkedTripsList = new ArrayList<LinkedTrips>();
        this.secondRouteId = linkedTrips.getsecondRouteId();
        
        this.cachedFirstRouteData = linkedTrips.getFirstStopData();
        this.cachedSecondRouteData = linkedTrips.getSecondStopData();
        this.linkedTripsList.add(linkedTrips);
    }
    
    /**
     * add the link trips to the list
     * @param linkedTrips
     */
    public void add(LinkedTrips linkedTrips){
        this.linkedTripsList.add(linkedTrips);
    }
    
    // <editor-fold desc=" Getters ">
    public String getFirstStopData(){
        return  this.secondRouteId + "," +
                this.linkedTripsList.size() + "," +
                this.cachedFirstRouteData;
    }
    
    public static String getFirstStopHeader(){
        return "SecondRouteId,Frequency_GroupedByRoutes,SecondStopId,FirstStopId,Lat,Lon\n";
    }
    
    public String getSecondStopData(){
        return  this.secondRouteId + "," +
                this.linkedTripsList.size() + "," +
                this.cachedSecondRouteData;
    }
    
    public static String getSecondStopHeader(){
        return "SecondRouteId,Frequency_GroupedByRoutes,FirstStopId,SecondStopId,Lat,Lon\n";
    }
    // </editor-fold>
}
