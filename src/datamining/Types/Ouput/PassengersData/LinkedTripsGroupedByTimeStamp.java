/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Ouput.PassengersData;

import datamining.Types.LinkedTrips;
import java.util.HashMap;

/**
 *
 * @author Mark
 */
public class LinkedTripsGroupedByTimeStamp {
    //<TimeStamp, LinkedTrips>
    private final HashMap<Long, LinkedTripsGroupedByFirstRoute> timeStampGroupedByFirstRouteMap;
    private int frequencyByTripBusStopKey;
    private String tripBusStopKey;
    
    public LinkedTripsGroupedByTimeStamp(LinkedTrips linkedTrips) {
        this.timeStampGroupedByFirstRouteMap = new HashMap<Long, LinkedTripsGroupedByFirstRoute>();
        this.frequencyByTripBusStopKey = 0;
        this.tripBusStopKey = linkedTrips.getTripBusStopKey();
        add(linkedTrips);
    }
    
    /**
     * This method is used to add LinkedTrips which will then be grouped by the
     * time stamp
     * @param linkedTrips
     */
    public void add(LinkedTrips linkedTrips){
        long timeStamp = linkedTrips.getTimeStamp();
        
        if(timeStampGroupedByFirstRouteMap.containsKey(timeStamp)){
            timeStampGroupedByFirstRouteMap.get(timeStamp).add(linkedTrips);
        }else{
            LinkedTripsGroupedByFirstRoute linkedTripsGroupedByFirstRoute = new LinkedTripsGroupedByFirstRoute(linkedTrips);
            timeStampGroupedByFirstRouteMap.put(timeStamp, linkedTripsGroupedByFirstRoute);
        }
        frequencyByTripBusStopKey++;
    }
    
    // <editor-fold desc=" Getters ">
    public String getRouteData(){
        return  this.tripBusStopKey + "," +
                this.frequencyByTripBusStopKey + ",";
    }
    
    public static String getRouteHeader(){
        return "TripBusStopKey,Frequency_ByTripBusStopKey,";
    }
    
    public HashMap<Long, LinkedTripsGroupedByFirstRoute> getTimeStampGroupedByFirstRouteMap() {
        return timeStampGroupedByFirstRouteMap;
    }
    // </editor-fold>
}
