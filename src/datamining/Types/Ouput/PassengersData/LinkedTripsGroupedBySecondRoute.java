/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Ouput.PassengersData;

import datamining.Types.LinkedTrips;
import java.util.HashMap;

/**
 *
 * @author Mark
 */
public class LinkedTripsGroupedBySecondRoute {
    //<Second route id, LinkedTripsFrequency>
    private final HashMap<Integer, LinkedTripsFrequency> sRouteLinkedTripsMap;
    private final int firstRouteId;
    private int frequencyByFirstRouteId;
    
    public LinkedTripsGroupedBySecondRoute(LinkedTrips linkedTrips) {
        this.sRouteLinkedTripsMap = new HashMap<Integer, LinkedTripsFrequency>();
        this.firstRouteId = linkedTrips.getfirstRouteId();
        this.frequencyByFirstRouteId = 0;
        add(linkedTrips);
    }
    
    /**
     * add the link trips to the list
     * @param linkedTrips
     */
    public void add(LinkedTrips linkedTrips){
        if(sRouteLinkedTripsMap.containsKey(linkedTrips.getsecondRouteId())){
            sRouteLinkedTripsMap.get(linkedTrips.getsecondRouteId()).add(linkedTrips);
        }else{
            LinkedTripsFrequency linkedTripsFrequency = new LinkedTripsFrequency(linkedTrips);
            sRouteLinkedTripsMap.put(linkedTrips.getsecondRouteId(), linkedTripsFrequency);
        }
        this.frequencyByFirstRouteId++;
    }
    
    // <editor-fold desc=" Getters ">
    public String getRouteData(){
        return  firstRouteId + ",";
    }
    
    public static String getRouteHeader(){
        return "FirstRouteID,";
    }
    
    public HashMap<Integer, LinkedTripsFrequency> getsRouteLinkedTripsMap() {
        return sRouteLinkedTripsMap;
    }
    // </editor-fold>
}
