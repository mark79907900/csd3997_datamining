/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types.Ouput.PassengersData;

import datamining.Types.LinkedTrips;
import datamining.DataMining;
import datamining.Types.Passenger;
import datamining.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class PassengerTripTransactions {
    //<timeStamp, Passenger>
    private final TreeMap<Long, Passenger> timeStampPassengerMap;
//    private final ArrayList<LinkedTrips> LinkedTrips;
    private final ArrayList<LinkedTrips> linkedTripsList;
    private final long cardNumber;
    
    public PassengerTripTransactions(Passenger passenger) {
        this.timeStampPassengerMap = new TreeMap<Long, Passenger>();
        this.linkedTripsList = new ArrayList<LinkedTrips>();
        cardNumber = passenger.getCardNumber();
        addPassenger(passenger);
    }
    
    /**
     * add a passenger transaction.
     * The passenger must have the same card number
     * @param passenger
     */
    public void addPassenger(Passenger passenger){
        try {
            //check that a transaction for the same person is being added
            if(passenger.getCardNumber() == cardNumber){
                
                /*
                Handles issues where the same passenger is punched more then one
                time due to a source system error.
                */
                if(!timeStampPassengerMap.containsKey(passenger.getTimeStamp())){
                    this.timeStampPassengerMap.put(passenger.getTimeStamp(), passenger);
                }
            }
            //if the passenger transaction is not of the same person
            else{
                ErrorHandling.throwException(
                        String.format(
                                "This transaction is not of the same passenger, "
                                        + "passed passenger transaction card number: %d "
                                        + "in transaction for passenger with card "
                                        + "number: %d",
                                passenger.getCardNumber(),
                                cardNumber
                        )
                );
            }
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * Use the Transactions data to detect the passengers which have re used
     * a different bus route within the stipulated time window
     */
    public ArrayList<LinkedTrips> detectLinkedTrips(){
        //<timeStamp, Passenger>
        for(Entry<Long, Passenger> passengerEntry :timeStampPassengerMap.entrySet()){
            long timeStamp = passengerEntry.getKey();
            
            /*
            get the next entry, ie the entry which has the next higher timestamp
            */
            Entry<Long, Passenger> nextPassengerEntry = timeStampPassengerMap.ceilingEntry(timeStamp+1);
            
            /*
            check for null, this is becuase the last value in the list will
            return a null value as it is the biggest value in the map
            
            remove situtions where both stops and route are equal as this is out
            of the scope of the algorithm
            */
            if(
                    nextPassengerEntry != null &&
                    passengerEntry.getValue().getPredictedStopId() != nextPassengerEntry.getValue().getPredictedStopId() &&
                    passengerEntry.getValue().getRouteId() != nextPassengerEntry.getValue().getRouteId()
                    ){
                long nextPassengerTripTimeStamp = nextPassengerEntry.getKey();
                
                if((nextPassengerTripTimeStamp-timeStamp) <= DataMining.config.getPropertyLong("secondBusTripTimeWindow")){
                    this.linkedTripsList.add(new LinkedTrips(passengerEntry.getValue(), nextPassengerEntry.getValue()));
                }
            }
        }
        return linkedTripsList;
    }
    
//    public ArrayList<LinkedTrips> getDetectedLinkedTrips(){
//        return LinkedTrips;
//    }
    
}
