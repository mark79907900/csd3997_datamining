/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types;

import datamining.Utils.ErrorHandling;
import java.util.logging.Level;
import datamining.DataMining;

/**
 *
 * @author Mark
 */
public class BusStopCoordinates {
    private int stopId;
    private String stopName;
    private double lat;
    private double lon;
    
    public BusStopCoordinates(String lineValue) {
        try{
            String[] parts = lineValue.split(",");
            
            //throw exception if the gps point passed does not have the expected number of columns
            int colCount =  DataMining.config.getPropertyInt("busStopCoordinatesDataset_NumberOfColumns");
            if(parts.length != colCount){
                ErrorHandling.throwException(
                        String.format("This BusStopCoordinates does not have %d columns, it has %d", colCount, parts.length)
                );
            }
            
            this.stopId = Integer.parseInt(parts[0]);
            this.stopName = parts[1];
            this.lat = Double.parseDouble(parts[2]);
            this.lon = Double.parseDouble(parts[3]);
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * @param stopId
     * @param lat
     * @param lon 
     */
    public BusStopCoordinates(int stopId, double lat, double lon) {
        try{
            this.stopId = stopId;
            this.stopName = "UnNamed";
            this.lat = lat;
            this.lon = lon;
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     */
    public BusStopCoordinates() {
        try{
            this.stopId = -1;
            this.stopName = "UnNamed";
            this.lat = 0.0;
            this.lon = 0.0;
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
//    public void setStopId(int stopId) {
//        this.stopId = stopId;
//    }
    
    // <editor-fold desc=" Getters ">
    public int getStopId() {
        return stopId;
    }
    
    public double getLat() {
        return lat;
    }
    
    public double getLon() {
        return lon;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
    
    @Override
    public String toString(){
        return "" + stopId + "," +
                stopName + "," +
                lat + "," +
                lon + "\n";
    }
    
    public static String toStingHeaders(){
        return "Stop ID,Stop Name,Lat,Lon\n";
    }
    
    public String toStringShort(){
        return "" + stopId + "," +
                lat + "," +
                lon + "\n";
    }
    
    public static String toStingShortTitles(){
        return "Stop ID,Lat,Lon\n";
    }
    // </editor-fold>
}
