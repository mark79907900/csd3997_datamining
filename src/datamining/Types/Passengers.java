/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining.Types;

import java.util.ArrayList;

/**
 *
 * @author Mark
 */
public class Passengers {
    private ArrayList<Passenger> passengerList;

    public Passengers(Passenger passenger) {
        passengerList = new ArrayList<Passenger>();
        passengerList.add(passenger);
    }
    
    public void addPassenger(Passenger passenger){
        passengerList.add(passenger);
    }
    
    public ArrayList<Passenger> getPassengerList(){
        return passengerList;
    }
}
