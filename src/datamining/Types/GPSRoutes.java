/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Types;

import datamining.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * Holds the routes and their functionality
 * @author Mark
 */
public class GPSRoutes {
    private ArrayList<GPSRoute> routeList;
    
    //key = lineId, Value=GPSPointsList Index
    private HashMap routeIndexMap;
    
    public GPSRoutes() {
        routeList = new ArrayList<GPSRoute>();
        routeIndexMap = new HashMap();
    }

    public ArrayList<GPSRoute> getRouteList() {
        return routeList;
    }
    
    /**
     * Add a row to the list of rows in the route specified by this row,
     * if the route does not exist, a new route is added
     * @param point
     */
    public void addRow(GPSRow point){
        try{
            //get the route id
            int routeID = point.getRouteID();
            
            //get the index of the route if it exists
            Object listIndex =routeIndexMap.get(routeID);
            
            //check if the route exists
            //if the route does not exist
            if(listIndex == null){
                
                //get the next index
                int index = routeList.size();
                
                //create a new route object
                GPSRoute route = new GPSRoute(routeID);
                //add the row to the new route object
                route.addRow(point);
                
                //add the route to the routeList
                routeList.add(route);
                
                //add the index to the route map
                routeIndexMap.put(routeID, index);
            }
            //if the route exists
            else{
                //get the route from the list
                GPSRoute route = routeList.get((int)listIndex);
                
                //add the row of data
                route.addRow(point);
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * get the route with the given lineID
     * @param lineID
     * @return 
     */
    public GPSRoute getRoute(int lineID){
        GPSRoute route = null;
        try{
            //get the index of the route if it exists
            Object listIndex =routeIndexMap.get(lineID);
            
            if(listIndex != null){
                //get the route from the list
                route = routeList.get((int)listIndex);
            }else{
                ErrorHandling.throwException(String.format("Route %d was not found", lineID));
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return route;
    }
    
    /**
     * Get a sorted ArrayList<Integer> with the bus routes in the dataset
     * @return 
     */
    public ArrayList<Integer> getRouteNumbersList(){
        ArrayList list = new ArrayList(
                Arrays.asList(routeIndexMap.keySet().toArray())
        );
        
        Collections.sort(list);
        return list;
    }
    
    /**
     * get a sorted Object[] with the bus routes in the dataset
     * @return 
     */
    public Object[] getRouteNumbersObjects(){
        Object[] list = routeIndexMap.keySet().toArray();
        Arrays.sort(list);
        return list;
    }
}
