/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Files;

import datamining.DataMining;
import datamining.Types.BusStopCoordinates;
import datamining.Types.GPSRoutes;
import datamining.Types.GPSRow;
import datamining.Types.Ouput.PassengersData.LinkedTripsFrequency;
import datamining.Types.Ouput.PassengersData.LinkedTripsGroupedByFirstRoute;
import datamining.Types.Ouput.PassengersData.LinkedTripsGroupedBySecondRoute;
import datamining.Types.Ouput.PassengersData.LinkedTripsGroupedByTimeStamp;
import datamining.Types.Passenger;
import datamining.Types.Ouput.StopsData.StopsDataHourlyOutput;
import datamining.Types.Ouput.StopsData.StopsDataOutput;
import datamining.Types.RoutePassengers;
import datamining.Types.RouteSchedule;
import datamining.Utils.ErrorHandling;
import datamining.Utils.TimeConvertor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Files {
    
    /**
     * Make a new directory with the given filename
     * @param fileName
     */
    public static String makeDirectory(String fileName) throws IOException{
        File folder = null;
        String outputFolderName = null;
        try {
            // if the file name passed is null/empty, use current time
            if(fileName == null){
                fileName = TimeConvertor.fileNameNow();
            }
            else if (fileName.isEmpty()){
                fileName = TimeConvertor.fileNameNow();
            }
            
            String writeDirectory = DataMining.config.getPropertyString("writeDirectory");
            String folderName = writeDirectory + fileName;
            
            folder = new File(folderName);
            
            int folderIndex = 0;
            
            //loop until an avialable name is found
            while(folder.exists()){
                folderIndex++;
                outputFolderName = String.format("%s_(%d)", folderName, folderIndex);
                folder = new File(outputFolderName);
            }
            //make a new folder
            folder.mkdir();
            
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return folder.getPath();
    }
    
    /**
     * Read the GPSRoutes from the path provided
     * @param path
     * @return
     * @throws IOException
     */
    public static GPSRoutes readGPSDataset(String path) throws IOException{
        
        //array used to hold the maze points
        GPSRoutes routes = new GPSRoutes();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(path));
            
            String line = br.readLine();
            
            int index = 0;
            while (line != null) {
                if(index > 1){
                    GPSRow row = new GPSRow(line);
                    routes.addRow(row);
                }
                line = br.readLine();
                index++;
            }
        } catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        return routes;
    }
    
    /**
     * Load the bus stop coordinates data and return a map with the bus stop id
     * and bus stop coordinates objects. This method checks that each bus stop
     * is uniquely listed in the path provided. The map has the bus stop id as
     * index and the bus stop coordinates object as value.
     * <Bus Stop ID, BusStopCoordinates>
     * @param path
     * @return
     * @throws IOException
     */
    public static HashMap<Integer, BusStopCoordinates> readBusStopCoordinates(String path) throws IOException{
        
        //array used to hold the maze points
        HashMap<Integer, BusStopCoordinates> busStopCoordinatesMap = new HashMap<Integer, BusStopCoordinates>();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(path));
            
            String line = br.readLine();
            
            int index = 0;
            while (line != null) {
                if(index > 0){
                    BusStopCoordinates busStop = new BusStopCoordinates(line);
                    
                    //check that there are no repeated bus stop ids
                    if(busStopCoordinatesMap.containsKey(busStop.getStopId())){
                        ErrorHandling.throwException(String.format("Bus Stop with Id %d already exists, skipping this row", busStop.getStopId()));
                    }
                    
                    busStopCoordinatesMap.put(busStop.getStopId(), busStop);
                }
                line = br.readLine();
                index++;
            }
        } catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        return busStopCoordinatesMap;
    }
    
    /**
     * Load the schedule for each bus route. This returns a map where the
     * index is the route id and the value contains the schedule for that
     * particular route <Route ID, Schedule>
     * @param path
     * @param busStopCoordinatesMap
     * @return
     * @throws IOException
     */
    public static HashMap<Integer, RouteSchedule> readSchedule(String path, HashMap<Integer, BusStopCoordinates> busStopCoordinatesMap) throws IOException{
        
        //array used to hold the bus stop corrdinate map points <RouteId, RouteSchedule>
        HashMap<Integer, RouteSchedule> routeScheduleMap = new HashMap<Integer, RouteSchedule>();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            
            int index = 0;
            while (line != null) {
                if(index > 0){
                    
                    String[] parts = line.split(",");
                    
                    //throw exception if the gps point passed does not have the expected number of columns
                    int colCount =  DataMining.config.getPropertyInt("routeScheduleDataset_NumberOfColumns");
                    if(parts.length != colCount){
                        ErrorHandling.throwException(
                                String.format("This RouteSchedule does not have the expected number of columns (%d), %d columns where read", colCount, parts.length)
                        );
                    }
                    
                    int RouteId = Integer.parseInt(parts[0]);
                    int stopId = Integer.parseInt(parts[1]);
                    
                    //get the route schedule from the map, null is returned if the current route does not exist
                    RouteSchedule routeSchedule = routeScheduleMap.get(RouteId);
                    
                    //if the route exists, add the new stop id
                    if(routeSchedule != null){
                        routeSchedule.addBusStopRoute(stopId);
                    }
                    //if the route does not exists, create a new route and add the stop id
                    else{
                        routeScheduleMap.put(RouteId, new RouteSchedule(busStopCoordinatesMap, stopId, RouteId));
                    }
                }
                line = br.readLine();
                index++;
            }
        } catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        return routeScheduleMap;
    }
    
    /**
     * This method will read the passenger list from path and return a map where
     * the key is the route id and the value is RoutePassengers. RoutePassengers
     * holds a list of passengers <route id, RoutePassengers>
     * @param path
     * @return
     * @throws IOException
     */
    public static HashMap<Integer, RoutePassengers> readPassengerDataset(String path) throws IOException{
        
        //array used to hold the maze points <RouteId, RoutePassengers>
        HashMap<Integer, RoutePassengers> passengerRouteMap = new HashMap<Integer, RoutePassengers>();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            
            int index = 0;
            while (line != null) {
                if(index > 1){
                    String[] parts = line.split(",");
                    
                    //create a new passenger object
                    Passenger passenger = new Passenger(line);
                    
                    /*
                    check if the route exists in the map, if it does add the
                    passenger to the list
                    */
                    if(passengerRouteMap.containsKey(passenger.getRouteId())){
                        passengerRouteMap.get(passenger.getRouteId()).addPassenger(passenger);
                    }
                    //if the route does not exist create a new route
                    else{
                        passengerRouteMap.put(passenger.getRouteId(), new RoutePassengers(passenger));
                    }
                    
                }
                line = br.readLine();
                index++;
            }
        } catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        return passengerRouteMap;
    }
    
    
    public static void dumpStopsDataOutput(String fileName, ArrayList<HashMap<Integer, StopsDataOutput>> outputList) throws IOException{
        BufferedWriter bufferedWriter = null;
        try{
            //output the file with stops
            File stopsFile = new File(
                    String.format("%s_BusStop%s",
                            fileName,
                            DataMining.config.getPropertyString("wirteSetExtension")
                    )
            );
            
            bufferedWriter = new BufferedWriter(new FileWriter(stopsFile));
            bufferedWriter.write(StopsDataOutput.toStringTitles()+StopsDataHourlyOutput.toStringTitles());
            
            for(HashMap<Integer, StopsDataOutput> outputMap: outputList){
                
                //<stop id, output>
                for(Entry<Integer, StopsDataOutput> stopEntry: outputMap.entrySet()){
                    StopsDataOutput output = stopEntry.getValue();
                    
                    //<TimeStamp, HourlyOutput>
                    for(Entry<Long, StopsDataHourlyOutput> hourlyStops: output.getHourlyOutputMap().entrySet()){
                        bufferedWriter.write(output.toString()+hourlyStops.getValue().toString());
                    }
                }
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( bufferedWriter != null ) bufferedWriter.close();
        }
    }
    
    public static void dumpDirectRouteDataOutput(String fileName, HashMap<String, LinkedTripsGroupedByTimeStamp> LinkedTripsOutput) throws IOException{
        BufferedWriter bufferedWriterFirstStop = null;
        BufferedWriter bufferedWriterSecondStop = null;
        
        try{
            //output the file with stops
            File firstStop = new File(
                    String.format("%s_DirectRoutes_s1%s",
                            fileName,
                            DataMining.config.getPropertyString("wirteSetExtension")
                    )
            );
            
            bufferedWriterFirstStop = new BufferedWriter(new FileWriter(firstStop));
            bufferedWriterFirstStop.write(
                    LinkedTripsGroupedByTimeStamp.getRouteHeader()+
                            LinkedTripsGroupedByFirstRoute.getRouteHeader()+
                            LinkedTripsGroupedBySecondRoute.getRouteHeader() +
                            LinkedTripsFrequency.getFirstStopHeader()
            );
            
            
            //output the file with stops
            File secondStop = new File(
                    String.format("%s_DirectRoutes_s2%s",
                            fileName,
                            DataMining.config.getPropertyString("wirteSetExtension")
                    )
            );
            
            bufferedWriterSecondStop = new BufferedWriter(new FileWriter(secondStop));
            bufferedWriterSecondStop.write(
                    LinkedTripsGroupedByTimeStamp.getRouteHeader()+
                            LinkedTripsGroupedByFirstRoute.getRouteHeader()+
                            LinkedTripsGroupedBySecondRoute.getRouteHeader() +
                            LinkedTripsFrequency.getSecondStopHeader()
            );
            
            
            
            //loop in the whole list of linked trips
            //<tripBusStopKey, LinkedTripsGroupedByTimeStamp>
            for(Entry<String, LinkedTripsGroupedByTimeStamp> linkedTripsOutputEntry: LinkedTripsOutput.entrySet()){
                LinkedTripsGroupedByTimeStamp linkedTripsGroupedByTimeStamp = linkedTripsOutputEntry.getValue();
                HashMap<Long, LinkedTripsGroupedByFirstRoute> linkedTripsGroupedByFirstRouteMap = linkedTripsGroupedByTimeStamp.getTimeStampGroupedByFirstRouteMap();
                
                //<TimeStamp, LinkedTrips>
                for(Entry<Long, LinkedTripsGroupedByFirstRoute> linkedTripsGroupedByFirstRouteEntry: linkedTripsGroupedByFirstRouteMap.entrySet()){
                    LinkedTripsGroupedByFirstRoute linkedTripsGroupedByFirstRoute = linkedTripsGroupedByFirstRouteEntry.getValue();
                    HashMap<Integer, LinkedTripsGroupedBySecondRoute> linkedTripsGroupedBySecondRouteMap = linkedTripsGroupedByFirstRoute.getfRouteLinkedTripsMap();
                    
                    //<First route id, LinkedTripsFrequency>
                    for(Entry <Integer, LinkedTripsGroupedBySecondRoute> linkedTripsGroupedBySecondRouteEntry :linkedTripsGroupedBySecondRouteMap.entrySet()){
                        LinkedTripsGroupedBySecondRoute linkedTripsGroupedBySecondRoute = linkedTripsGroupedBySecondRouteEntry.getValue();
                        HashMap<Integer, LinkedTripsFrequency> linkedTripsFrequencyMap = linkedTripsGroupedBySecondRoute.getsRouteLinkedTripsMap();
                        
                        //<Second route id, LinkedTripsFrequency>
                        for(Entry<Integer, LinkedTripsFrequency> linkedTripsFrequencyEntry:linkedTripsFrequencyMap.entrySet()){
                            LinkedTripsFrequency linkedTripsFrequency = linkedTripsFrequencyEntry.getValue();
                            
                            String first = linkedTripsGroupedByTimeStamp.getRouteData() +
                                            linkedTripsGroupedByFirstRoute.getRouteData() +
                                            linkedTripsGroupedBySecondRoute.getRouteData() +
                                            linkedTripsFrequency.getFirstStopData();
                            
                            bufferedWriterFirstStop.write(first);
                            
                            String second = linkedTripsGroupedByTimeStamp.getRouteData() +
                                            linkedTripsGroupedByFirstRoute.getRouteData() +
                                            linkedTripsGroupedBySecondRoute.getRouteData() +
                                            linkedTripsFrequency.getSecondStopData();
                            
                            bufferedWriterSecondStop.write(second);
                            
                        }
                    }
                    
                }
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( bufferedWriterFirstStop != null ) bufferedWriterFirstStop.close();
            if ( bufferedWriterSecondStop != null ) bufferedWriterSecondStop.close();
        }
    }
}
