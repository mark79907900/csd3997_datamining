/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Files;

import datamining.DataMining;
import datamining.Types.Ouput.StopsData.StopsDataHourlyOutput;
import datamining.Types.Ouput.StopsData.StopsDataOutput;
import datamining.Utils.ErrorHandling;
import datamining.Utils.TimeConvertor;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mark
 */
public class AuditLog {
    private static String fileName = "";
    
    public static void setFileName (String fileName){
        AuditLog.fileName = fileName;
    }
    
    public static void error(Exception e) throws IOException{
        PrintWriter printWriter = null;
        FileWriter fileWriter = null;
        
        try{
            //output the file with stops
            File auditLog = new File(String.format("%s%s", fileName, "_AuditLog.txt"));
            
            if(!auditLog.exists()){
                printWriter = new PrintWriter(new FileWriter(auditLog));
            }else{
                fileWriter = new FileWriter(auditLog,true);
                printWriter = new PrintWriter(fileWriter);
            }
            
            printWriter.println(TimeConvertor.now());
            
            int debugLevel = DataMining.config.getPropertyInt("debugLevel");
            
            if(debugLevel >= 3){
                e.printStackTrace(printWriter);
            }else{
                printWriter.println(e.getMessage());
            }
            printWriter.println();
            
        }catch(Exception ex){
            Logger logger = Logger.getAnonymousLogger();
            logger.log(Level.SEVERE ,"Exception :: " , ex);
        } finally {
            if ( printWriter != null ) printWriter.close();
            if (fileWriter != null) fileWriter.close();
        }
    }
    
    public static void results(String text, boolean printToConsole) throws IOException{
        BufferedWriter bufferedWriter = null;
        FileWriter fileWriter = null;
        try{
            //output the file with stops
            File file = new File(String.format("%s%s", fileName, "_ResultsAudit.txt"));
            
            if(!file.exists()){
                bufferedWriter = new BufferedWriter(new FileWriter(file));
            }else{
                fileWriter = new FileWriter(file,true);
                bufferedWriter = new BufferedWriter(fileWriter);
            }
            
            bufferedWriter.write(
                    String.format(
                            "%s: %s\n",
                            TimeConvertor.now(),
                            text.replace("\n", "")
                    )
            );
            
            if(printToConsole){
                System.out.println(text);
            }
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if (bufferedWriter != null) bufferedWriter.close();
            if (fileWriter != null) fileWriter.close();
        }
    }
}
