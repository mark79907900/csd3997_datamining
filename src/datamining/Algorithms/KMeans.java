/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Algorithms;

import datamining.Types.BusStopCoordinates;
import datamining.Types.GPSRoute;
import datamining.Types.GPSRoutes;
import datamining.Types.GPSRow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author Mark
 */
public class KMeans {
    private final GPSRoutes GPSDataset;
    private HashMap<Integer, BusStopCoordinates> busStopCoordinatesMap;
    
    public KMeans(GPSRoutes GPSDataset, HashMap<Integer, BusStopCoordinates> busStopCoordinatesMap) {
        this.GPSDataset = GPSDataset;
        this.busStopCoordinatesMap = busStopCoordinatesMap;
        execute();
    }
    
    private void execute(){
        HashMap<Integer, ArrayList<GPSRow>> busStopList = new HashMap<Integer, ArrayList<GPSRow>>();
        
        //group all of the gps coordinates by bus stop id
        for(GPSRoute route: GPSDataset.getRouteList()){
            for(GPSRow row :route.getGPSRowList()){
                
                if(row.getStopID() != -1){
                    //if bus stop id exists, appened the row to the list
                    if(busStopList.containsKey(row.getStopID())){
                        busStopList.get(row.getStopID()).add(row);
                    }
                    /*
                    if bus stop id does not exists, create a new
                    row and appened the row to the list
                    */
                    else{
                        ArrayList<GPSRow> list = new ArrayList<GPSRow>();
                        list.add(row);
                        busStopList.put(row.getStopID(), list);
                    }
                }
                
            }
        }
        
        //for every route
        for(Entry<Integer, ArrayList<GPSRow>> gpsRowEntry: busStopList.entrySet()){
            double latTotal = 0.0;
            double lonTotal = 0.0;
            
            int stopId = gpsRowEntry.getKey();
            BusStopCoordinates busStopCoordinates = busStopCoordinatesMap.get(stopId);
            
            ArrayList<GPSRow> stopList = gpsRowEntry.getValue();
            
            for(GPSRow row :stopList){
                latTotal += row.getLat();
                lonTotal += row.getLon();
            }
            
            if(busStopCoordinates != null){
                latTotal += busStopCoordinates.getLat();
                lonTotal += busStopCoordinates.getLon();
                
                latTotal = latTotal/(stopList.size()+1);
                lonTotal = lonTotal/(stopList.size()+1);
                
                busStopCoordinates.setLat(latTotal);
                busStopCoordinates.setLon(lonTotal);
            }else{
                latTotal = latTotal/(stopList.size()+1);
                lonTotal = lonTotal/(stopList.size()+1);
                
                busStopCoordinates = new BusStopCoordinates(stopId, latTotal, lonTotal);
            }
            
            busStopCoordinatesMap.put(stopId, busStopCoordinates);
        }
    }
}
