/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Algorithms.KNN;

import datamining.Types.GPSRow;

/**
 *
 * @author Mark
 */
public class GPSRowDistance {
    private final GPSRow trainingRow;
    private final double euclideanDistance;
    private double weight;
    
    public GPSRowDistance(GPSRow trainingRow, double euclideanDistance) {
        this.trainingRow = trainingRow;
        this.euclideanDistance = euclideanDistance;
        this.weight = 1/euclideanDistance;
    }
    
    public int getStopID() {
        return trainingRow.getStopID();
    }
    
    public double getDistance() {
        return euclideanDistance;
    }
    
    public double getWeight() {
        return weight;
    }
}
