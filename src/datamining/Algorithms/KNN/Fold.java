/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining.Algorithms.KNN;

import datamining.Types.GPSRow;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Mark
 */
public class Fold {
    private ArrayList<GPSRow> gpsRowList;

    public Fold() {
        this.gpsRowList = new ArrayList<GPSRow>();
    }
    
    public void addAll(List<GPSRow> subList){
        this.gpsRowList.addAll(subList);
    }
    
    public void shuffle(){
        Collections.shuffle(gpsRowList);;
    }

    public ArrayList<GPSRow> getGpsRowList() {
        return gpsRowList;
    }
    
}
