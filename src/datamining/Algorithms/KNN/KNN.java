/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Algorithms.KNN;

import datamining.Files.AuditLog;
import datamining.Types.BusStopCoordinates;
import datamining.Types.GPSRoute;
import datamining.Types.GPSRoutes;
import datamining.Types.GPSRow;
import datamining.Types.Passenger;
import datamining.Types.Passengers;
import datamining.Types.Results.Result;
import datamining.Types.Results.Results;
import datamining.Types.RoutePassengers;
import datamining.Types.RouteSchedule;
import datamining.Utils.Calculations;
import datamining.Utils.ErrorHandling;
import datamining.Utils.Profiler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class KNN {
    // <editor-fold desc=" DataSets ">
    private final GPSRoutes GPSDataset;
    private final HashMap<Integer, RouteSchedule> scheduleMap;
    private final HashMap<Integer, RoutePassengers> passengerDatasetMap;
    // </editor-fold>
    
    // <editor-fold desc=" Constants ">
    private final int k;
    private final int numberOfFolds;
    private final int busStopDistanceThreshold;
    private final int trafficDistanceThreshold;
    // </editor-fold>
    
    private Results gpsResults;
    private Results passengerResults;
    
    //<Fold id, Fold>
    private HashMap<Integer, Fold> foldMap;
    private ArrayList<GPSRow> trainingSet;
    private ArrayList<GPSRow> testingSet;
    
    /**
     *
     * @param GPSDataset
     * @param scheduleMap
     * @param passengerDatasetMap
     * @param k
     * @param numberOfFolds
     * @param busStopDistanceThreshold
     * @param trafficDistanceThreshold
     */
    public KNN(GPSRoutes GPSDataset,
            HashMap<Integer, RouteSchedule> scheduleMap,
            HashMap<Integer, RoutePassengers> passengerDatasetMap,
            int k,
            int numberOfFolds,
            int busStopDistanceThreshold,
            int trafficDistanceThreshold
    ) {
        // <editor-fold desc=" Datasets and constants ">
        this.GPSDataset = GPSDataset;
        this.scheduleMap = scheduleMap;
        this.passengerDatasetMap=passengerDatasetMap;
        this.busStopDistanceThreshold=busStopDistanceThreshold;
        this.trafficDistanceThreshold=trafficDistanceThreshold;
        this.k = k;
        this.numberOfFolds = numberOfFolds;
        // </editor-fold>
        
        // <editor-fold desc=" Results ">
        this.gpsResults = new Results(true);
        this.passengerResults = new Results(false);
        // </editor-fold>
        
        this.foldMap = new HashMap<Integer, Fold>();
        this.trainingSet = new ArrayList<GPSRow>();
        this.testingSet = new ArrayList<GPSRow>();
        
        divideIntoFolds();
        execute();
    }
    
    /**
     * split the dataset into N number of folds
     */
    private void divideIntoFolds(){
        try {
            //<bus stop id, ArrayList<GPSRow>>
            HashMap<Integer, ArrayList<GPSRow>> busStopList = new HashMap<Integer, ArrayList<GPSRow>>();
            
            //group all of the gps coordinates by bus stop id
            for(GPSRoute route: GPSDataset.getRouteList()){
                for(GPSRow row :route.getGPSRowList()){
                    
                    //if bus stop id exists, appened the row to the list
                    if(busStopList.containsKey(row.getStopID())){
                        busStopList.get(row.getStopID()).add(row);
                    }
                    /*
                    if bus stop id does not exists, create a new
                    row and appened the row to the list
                    */
                    else{
                        ArrayList<GPSRow> list = new ArrayList<GPSRow>();
                        list.add(row);
                        busStopList.put(row.getStopID(), list);
                    }
                }
            }
            
            //create the folds
            for(int i=0; i<this.numberOfFolds; i++){
                foldMap.put(i, new Fold());
            }
            
            //place the gps rows in the folds
            for(Entry<Integer, ArrayList<GPSRow>> gpsRowEntry:busStopList.entrySet()){
                ArrayList<GPSRow> gpsRowList = gpsRowEntry.getValue();
                int numberOfRows = gpsRowList.size();
                int numberOfRowsPerFold = numberOfRows/this.numberOfFolds;
                
                //shuffle the list
                Collections.shuffle(gpsRowList);
                
                for(Entry<Integer, Fold> foldEntry:foldMap.entrySet()){
                    int foldIndex = foldEntry.getKey();
                    int startIndex = foldIndex * numberOfRowsPerFold;
                    
                    if((foldIndex+1) < numberOfFolds){
                        int endIndex = (foldIndex+1) * numberOfRowsPerFold;
                        foldEntry.getValue().addAll(gpsRowList.subList(startIndex, endIndex));
                    }else{
                        foldEntry.getValue().addAll(gpsRowList.subList(startIndex, gpsRowList.size()-1));
                    }
                }
            }
            
            //shuffle all of the points in each fold
            for(Entry<Integer, Fold> foldEntry:foldMap.entrySet()){
                foldEntry.getValue().shuffle();
            }
            
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    
    private void execute(){
        Profiler profiler = new Profiler(true);
        try {
            for(int i=0; i<this.numberOfFolds; i++){
                AuditLog.results(String.format("Evaluating Fold %d...", i+1), true);
                distributeSet(i);
                
                for(GPSRow testRow :this.testingSet){
                    int k = this.k;
                    
                    RouteSchedule routeSchedule = scheduleMap.get(testRow.getRouteID());
                    
                    // <editor-fold desc="Route Result">
                    Result gpsResult = gpsResults.getResult(routeSchedule);
                    Result passengerResult = passengerResults.getResult(routeSchedule);
                    // </editor-fold>
                    
                    // <editor-fold desc=" Classify Bus Stop ID ">
                    
                    ArrayList<GPSRowDistance> distanceList = new ArrayList<GPSRowDistance>();
                    
                    //calculate the distance between each gps row and the bus stop
                    for(GPSRow trainingRow :this.trainingSet){
                        distanceList.add(
                                new GPSRowDistance(
                                        trainingRow,
                                        Calculations.euclideanDistance(testRow, trainingRow)
                                )
                        );
                    }
                    
                    //sort by smallest distance first
                    sort(distanceList);
                    
                    /*
                    check if the k value passed is bigger then the size of the
                    training set
                    */
                    if(k > distanceList.size()){
                        k = distanceList.size();
                    }
                    
                    //create a sublist with the first K values
                    List<GPSRowDistance> distanceSubList = distanceList.subList(0, k);
                    
                    /*
                    Create an occurence map and use it to assign weights to each
                    gps row
                    */
                    HashMap<Integer, Double> occurrenceMap = new HashMap<Integer, Double>();
                    for(GPSRowDistance distanceRow : distanceSubList){
                        if(occurrenceMap.containsKey(distanceRow.getStopID())){
                            occurrenceMap.put(distanceRow.getStopID(), occurrenceMap.get(distanceRow.getStopID()) + distanceRow.getWeight());
                        }else{
                            occurrenceMap.put(distanceRow.getStopID(), distanceRow.getWeight());
                        }
                    }
                    
                    if(occurrenceMap.size() > 0){
                        //Find the stop id with the biggest weight
                        int predictedStopId = (int)Collections.max(occurrenceMap.entrySet(),
                                new Comparator<Map.Entry<Integer,Double>>(){
                                    @Override
                                    public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
                                        return o1.getValue() > o2.getValue()? 1:-1;
                                    }
                                }).getKey();
                        
                        // </editor-fold>
                        
                        // <editor-fold desc=" Classify Stop Type">
                        /*
                        if the predicted bus stop is [-1], this means that this
                        stop was a noise point, thus it will be set as noise
                        */
                        if(predictedStopId != -1){
                            //get the coordinates of the bus stop
                            BusStopCoordinates predictedBusStopCoordinates = routeSchedule.getBusStopCoordinates(predictedStopId);
                            
                            if(predictedBusStopCoordinates != null){
                                double geoDistance = Calculations.distanceBetweenGPSPositions(
                                        testRow.getLat(), testRow.getLon(),
                                        predictedBusStopCoordinates.getLat(), predictedBusStopCoordinates.getLon()
                                );
                                
                                //if it is within the stop threshold
                                if(geoDistance < busStopDistanceThreshold){
                                    testRow.setPredictedValues(predictedBusStopCoordinates, GPSRow.RowType.BUS_STOP);
                                    gpsResult.incrementBusStopCounter();
                                }
                                //if it is within traffic threshold
                                else if(geoDistance < trafficDistanceThreshold){
                                    testRow.setPredictedValues(predictedBusStopCoordinates, GPSRow.RowType.TRAFFIC_STOP);
                                    gpsResult.incrementTrafficCounter();
                                }
                                //set it to noise
                                else{
                                    testRow.setPredictedValues(predictedBusStopCoordinates, GPSRow.RowType.NOISE);
                                }
                            }else{
                                testRow.setPredictedValues(new BusStopCoordinates(), GPSRow.RowType.NOISE);
                            }
                        }else{
                            testRow.setPredictedValues(new BusStopCoordinates(), GPSRow.RowType.NOISE);
                        }
                        
                        gpsResult.addClassification(testRow.getStopID(), predictedStopId);
                        // </editor-fold>
                        
                        // <editor-fold desc=" Classify Passengers ">
                        if(testRow.getRowType() == GPSRow.RowType.BUS_STOP){
                            RoutePassengers routePassengers = passengerDatasetMap.get(routeSchedule.getRouteId());
                            Passengers passengers = routePassengers.getPassengers(testRow.getTimeStamp());
                            
                            /*
                            loop in the list of passengers and classify the
                            passengers associated with this busStop
                            */
                            for(Passenger passenger : passengers.getPassengerList()){
                                
                                /*
                                calculate distance of between the gps coordinates
                                of the passenger and the gps coordinates of the predicted
                                bus stop
                                */
                                double geoDistance = Calculations.distanceBetweenGPSPositions(
                                        passenger.getLat(), passenger.getLon(),
                                        testRow.getLat(), testRow.getLon()
                                );
                                
                                /*
                                check if the distance between the points is within
                                the threshold, if it is assign it to this stop
                                */
                                if(geoDistance <= busStopDistanceThreshold){
                                    passenger.setPredictedValues(testRow.getPredictedBusStopCoordinates());
                                    testRow.addPassenger(passenger);
                                    
                                    passengerResult.addClassification(passenger.getBusStopId(), testRow.getStopID());
                                }
                            }
                        }
                        // </editor-fold>
                    }
                }
            }
            profiler.printTimeTaken();
            AuditLog.results("Bus Stop Results", true);
            gpsResults.print(false, true);
            AuditLog.results("\nPassenger Results", true);
            passengerResults.print(false, true);
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    // <editor-fold desc=" Helper Functionality ">
    /**
     * Distribute the folds based on the index passed.
     * The index passed must be n-1 folds
     * The index of the fold passed as a parameters will be used as a testing set
     * @param testSetFoldIndex
     */
    private void distributeSet(int testSetFoldIndex){
        try {
            if(testSetFoldIndex < this.numberOfFolds){
                this.trainingSet.clear();
                this.testingSet.clear();
                
                for(Entry<Integer, Fold> foldEntry:foldMap.entrySet()){
                    int foldIndex = foldEntry.getKey();
                    
                    if(foldIndex == testSetFoldIndex){
                        this.testingSet.addAll(foldEntry.getValue().getGpsRowList());
                    }else{
                        this.trainingSet.addAll(foldEntry.getValue().getGpsRowList());
                    }
                }
            }
            else{
                ErrorHandling.throwException(String.format("fold index passed (%d) is out of bounds, skipping this test", testSetFoldIndex));
            }
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * method used to sort a multidimensional arraylist of Integer
     * @param columnIndex column which will be used for sorting
     * @param list list which will be sorted
     */
    private void sort(ArrayList<GPSRowDistance> distanceList) {
        Comparator<GPSRowDistance> myComparator = new Comparator<GPSRowDistance>() {
            @Override
            public int compare(GPSRowDistance o1, GPSRowDistance o2) {
                return Double.compare(o1.getDistance(), o2.getDistance());
            }
        };
        Collections.sort(distanceList, myComparator);
    }
    // </editor-fold>
}
