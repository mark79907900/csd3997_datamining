/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Algorithms;

import datamining.Files.AuditLog;
import datamining.Types.Results.Result;
import datamining.Types.Results.Results;
import datamining.Types.BusStopCoordinates;
import datamining.Types.GPSRoute;
import datamining.Types.GPSRoutes;
import datamining.Types.GPSRow;
import datamining.Types.Passenger;
import datamining.Types.Passengers;
import datamining.Types.RoutePassengers;
import datamining.Types.RouteSchedule;
import datamining.Utils.Calculations;
import datamining.Utils.ErrorHandling;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 * @author Mark
 */
public class ParzenWindow {
    // <editor-fold desc=" Datasets Attributes">
    private final GPSRoutes GPSDataset;
    private final HashMap<Integer, RouteSchedule> schedule;
    private final HashMap<Integer, RoutePassengers> passengerDataset;
    // </editor-fold>
    
    private final int busStopDistanceThreshold;
    private final int trafficDistanceThreshold;
    private Results gpsResults;
    private Results passengerResults;
    
    
    public ParzenWindow(GPSRoutes GPSDataset, HashMap<Integer, RouteSchedule> schedule, HashMap<Integer, RoutePassengers> passengerDataset, int busStopDistanceThreshold, int trafficDistanceThreshold) {
        this.GPSDataset = GPSDataset;
        this.schedule = schedule;
        this.passengerDataset = passengerDataset;
        this.busStopDistanceThreshold = busStopDistanceThreshold;
        this.trafficDistanceThreshold = trafficDistanceThreshold;
        this.gpsResults = new Results(true);
        this.passengerResults = new Results(false);
        
        try {
            if(busStopDistanceThreshold < trafficDistanceThreshold){
                execute();
            }else{
                ErrorHandling.throwException("Bus stop distance threshold should be smaller then traffic distance threshold");
            }
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    private void execute(){
        try {
            //loop every route
            for(GPSRoute route :GPSDataset.getRouteList()){
                
                AuditLog.results(String.format("Evaluating Route %d...", route.getRouteID()), true);
                
                //get the bus stops present in every route
                RouteSchedule routeSchedule = schedule.get(route.getRouteID());
                
                // <editor-fold desc=" Result Objects ">
                Result gpsResult = gpsResults.getResult(routeSchedule);
                Result passengerResult = passengerResults.getResult(routeSchedule);
                // </editor-fold>
                
                //loop the gps points for every route
                for(GPSRow gpsRow :route.getGPSRowList()){
                    
                    //<bus stop id, votes>
                    // <editor-fold desc="Occurrence Maps">
                    HashMap<Integer, Double> busStopOccurrence = new HashMap<Integer, Double>();
                    HashMap<Integer, Double> trafficOccurrence = new HashMap<Integer, Double>();
                    // </editor-fold>
                    
                    BusStopCoordinates busStopCoordinates = null;
                    
                    //loop all of the bus stops in this route
                    for(Entry<Integer, BusStopCoordinates> busStopsMap:  routeSchedule.getBusStopsMap().entrySet()){
                        
                        //get bus stop coordinates
                        busStopCoordinates = busStopsMap.getValue();
                        
                        //calculate distance of gps data point from the bus stop
                        double geoDistance = Calculations.distanceBetweenGPSPositions(
                                gpsRow.getLat(), gpsRow.getLon(), 
                                busStopCoordinates.getLat(), busStopCoordinates.getLon()
                        );
                        
                        // <editor-fold desc="Assign it to the specified category based on the thresholds">
                        //if it is within the stop threshold
                        if(geoDistance <= busStopDistanceThreshold){
                            votingMechanism(busStopOccurrence, busStopCoordinates, geoDistance);
                        }
                        //if it is within traffic threshold
                        else if(geoDistance <= trafficDistanceThreshold){
                            votingMechanism(trafficOccurrence, busStopCoordinates, geoDistance);
                        }
                        //if it is within no threshold, it is classifed as noise
                        else{
                            gpsRow.setRowType(GPSRow.RowType.NOISE);
                        }
                        // </editor-fold>
                    }
                    
                    /*
                    Update GPS row and results
                    */
                    // <editor-fold desc=" Bus Stop ">
                    if(!busStopOccurrence.isEmpty()){
                        //Find the key(digit) with the biggest occurance
                        int stopId = findMaxOccurrence(busStopOccurrence);
                        
                        //get the bus stop coordinates
                        busStopCoordinates = routeSchedule.getBusStopsMap().get(stopId);
                        gpsRow.setPredictedValues(busStopCoordinates, GPSRow.RowType.BUS_STOP);
                        
                        gpsResult.addClassification(gpsRow.getStopID(), stopId);
                        gpsResult.incrementBusStopCounter();
                    }
                    
                    // </editor-fold>
                    
                    // <editor-fold desc=" Traffic Stop ">
                    if(!trafficOccurrence.isEmpty()){
                        int stopIdTraffic = findMaxOccurrence(trafficOccurrence);
                        
                        //get the bus stop coordinates
                        busStopCoordinates = routeSchedule.getBusStopsMap().get(stopIdTraffic);
                        gpsRow.setPredictedValues(busStopCoordinates, GPSRow.RowType.TRAFFIC_STOP);
                        gpsResult.incrementTrafficCounter();
                        gpsResult.addClassification(gpsRow.getStopID(), stopIdTraffic);
                    }
                    // </editor-fold>
                    
                    // <editor-fold desc=" Passengers ">
                    if(gpsRow.getRowType() == GPSRow.RowType.BUS_STOP){
                        RoutePassengers routePassengers = passengerDataset.get(route.getRouteID());
                        Passengers passengers = routePassengers.getPassengers(gpsRow.getTimeStamp());
                        
                        /*
                        loop in the list of passengers and classify the
                        passengers associated with this busStop
                        */
                        for(Passenger passenger : passengers.getPassengerList()){
                            
                            /*
                            calculate distance of between the gps coordinates
                            of the passenger and the gps coordinates of the predicted
                            bus stop
                            */
                            double geoDistance = Calculations.distanceBetweenGPSPositions(
                                    passenger.getLat(), passenger.getLon(), 
                                    gpsRow.getLat(), gpsRow.getLon()
                            );
                            
                            /*
                            check if the distance between the points is within
                            the threshold, if it is assign it to this stop
                            */
                            if(geoDistance < busStopDistanceThreshold){
                                passenger.setPredictedValues(busStopCoordinates);
                                gpsRow.addPassenger(passenger);
                                
                                passengerResult.addClassification(passenger.getBusStopId(), gpsRow.getStopID());
                            }
                        }
                    }
                    // </editor-fold>
                }
            }
            AuditLog.results("\nBus Stop Results", true);
            gpsResults.print(false, true);
            AuditLog.results("\nPassenger Results", true);
            passengerResults.print(false, true);
            
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * method used to add votes to the closest stop
     * @param occurrenceMap
     * @param busStopCoordinates
     */
    private void votingMechanism(HashMap<Integer, Double> occurrenceMap, BusStopCoordinates busStopCoordinates, double distance){
        if(occurrenceMap.containsKey(busStopCoordinates.getStopId())){
            occurrenceMap.put(busStopCoordinates.getStopId(), occurrenceMap.get(busStopCoordinates.getStopId()) + (1/distance));
        }else{
            occurrenceMap.put(busStopCoordinates.getStopId(), (1/distance));
        }
    }
    
    /**
     * Find the value of a map which has the maximum occurrence
     * @param occurrenceMap
     * @return
     */
    private int findMaxOccurrence(HashMap<Integer, Double> occurrenceMap){
        if(!occurrenceMap.isEmpty()){
            return (int)Collections.max(occurrenceMap.entrySet(),
                    new Comparator<Entry<Integer,Double>>(){
                        @Override
                        public int compare(Entry<Integer, Double> o1, Entry<Integer, Double> o2) {
                            return o1.getValue() > o2.getValue()? 1:-1;
                        }
                    }).getKey();
        }
        return -1;
    }
    
    
    
}
