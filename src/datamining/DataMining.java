/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining;

import datamining.Algorithms.ParzenWindow;
import datamining.Algorithms.KMeans;
import datamining.Algorithms.KNN.KNN;
import datamining.Files.AuditLog;
import datamining.Types.Ouput.ProcessOutput;
import datamining.Utils.Config;
import datamining.Files.Files;
import datamining.GUI.MainGui;
import datamining.Types.BusStopCoordinates;
import datamining.Types.GPSRoutes;
import datamining.Types.RoutePassengers;
import datamining.Types.RouteSchedule;
import datamining.Utils.ErrorHandling;
import datamining.Utils.Profiler;
import java.util.HashMap;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class DataMining {
    public static Config config = new Config("config.properties");
    public static Config gui = new Config("gui.properties");
    public static Config lang = new Config("lang.properties");
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            
            MainGui.init();
//            // <editor-fold desc=" Create new folder and set file name ">
//            //todo - get this from gui
//            String fileName = "ff";
//            String outputFileName = "";
//            
//            //Make folder
//            String folderName = Files.makeDirectory(fileName);
//            
//            //check that the folder was created successfully
//            if(folderName != null){
//                outputFileName = String.format("%s\\%s", folderName, fileName);
//            }
//            
//            AuditLog.setFileName(outputFileName);
//            // </editor-fold>
//            
//            Profiler profiler = new Profiler(false);
//            
//            // <editor-fold desc=" Load DataSets ">
//            AuditLog.results("Loading...", true);
//            profiler.reset();
//            
//            GPSRoutes GPSDataset = Files.readGPSDataset("6_7_GPSPoints.csv");
//            HashMap<Integer, BusStopCoordinates> busStopCoordinates = Files.readBusStopCoordinates("6_7_Stops.csv");
//            //KMeans kmeans = new KMeans(GPSDataset, busStopCoordinates);
//            HashMap<Integer, RouteSchedule> schedule = Files.readSchedule("6_7_RouteStopsMap.csv", busStopCoordinates);
//            HashMap<Integer, RoutePassengers> passengerDataset = Files.readPassengerDataset("6_7_Passengers.csv");
//            
//            profiler.printTimeTaken();
//            // </editor-fold>
//            
//            // <editor-fold desc=" Process Algorithm ">
//            AuditLog.results("\nProcessing...", true);
//            profiler.reset();
//            
//            //KNN knn = new KNN(GPSDataset, schedule, passengerDataset, 5, 10, 10, 50);
//            ParzenWindow a = new ParzenWindow(GPSDataset, schedule, passengerDataset, 20, 50);
//            
//            profiler.printTimeTaken();
//            // </editor-fold>
//            
//            // <editor-fold desc=" Cleaning ">
//            AuditLog.results("\nCleaning Results...", true);
//            profiler.reset();
//            ProcessOutput po = new ProcessOutput(GPSDataset, passengerDataset, schedule);
//            profiler.printTimeTaken();
//            // </editor-fold>
//            
//            // <editor-fold desc=" Saving ">
//            AuditLog.results(String.format("\nSaving results as <%s>...", outputFileName.split("\\\\")[1]), true);
//            profiler.reset();
//            Files.dumpStopsDataOutput(outputFileName, po.getRouteOutputList());
//            Files.dumpDirectRouteDataOutput(outputFileName, po.getLinkedTripsOutput());
//            profiler.printTimeTaken();
//            // </editor-fold>
            
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
}
