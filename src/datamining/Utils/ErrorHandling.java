/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Utils;
import datamining.DataMining;
import datamining.Files.AuditLog;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This Class ensures that all the errors are handled the same, thus having a centralised error handling unit
 * @author Mark
 */
public class ErrorHandling {
    
    /**
     * This method is used to process exceptions
     * @param level
     * @param ex
     */
    public static void output(Level level, Exception ex){
        try{
            AuditLog.error(ex);
            throw ex;
        }
        catch(Exception e){
            int debugLevel = DataMining.config.getPropertyInt("debugLevel");
            if(debugLevel >= 3){
                Logger logger = Logger.getAnonymousLogger();
                logger.log(level ,"Exception :: " , e);
            }else if (debugLevel == 2){
                System.out.println(e.getMessage());
            }
        }
    }
    
    /**
     * This method is used to notify a logic error
     * @param msg
     */
    public static void notifyError(String msg){
        System.err.println(String.format("Warning: %s", msg));
    }
    
    /**
     * Throw exception with the passed message
     * @param msg
     * @throws Exception
     */
    public static void throwException(String msg) throws Exception{
        throw new Exception(msg);
    }
    
}
