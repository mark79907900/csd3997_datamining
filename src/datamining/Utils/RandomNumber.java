/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Utils;

import java.util.Random;

/**
 * Generates a random integer between min and max
 * @author Mark
 */
public class RandomNumber {
    private static Random random = new Random();
    
    /**
     * get random int between min and max
     * @param min
     * @param max
     * @return
     */
    public static int getRandomInt(int min, int max){
        int output = 0;
        if(min <= max){
            output = random.nextInt(max - min + 1) + min;
        }
        return output;
    }
    
    /**
     * get random long between min and max
     * @param min
     * @param max
     * @return
     */
    public static long getRandomLong(long min, long max){
        long output = 0;
        if(min <= max){
            output = min+((long)(random.nextDouble()*(max-min)));
        }
        return output;
    }
}
