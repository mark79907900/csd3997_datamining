package datamining.Utils;

import datamining.Files.AuditLog;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Profiler {
    private long startTime;
    
    /**
     * Profiler Constructor
     * The start parameter determines if the timer should start when the object is constructed
     * @param start
     */
    public Profiler(boolean start) {
        if (start){
            this.startTime = System.nanoTime();
        }
    }
    
    /**
     * Start the timer.
     * This could be used to reset the timer
     */
    public void reset(){
        this.startTime = System.nanoTime();
    }
    
    /**
     * print the difference from the start
     */
    public void printTimeTaken(){
        System.out.println(diffTime());
    }
    
    /**
     * get the difference from the start
     */
    public long diff(){
        return System.nanoTime() - this.startTime;
    }
    
    /**
     * print the time taken in Minutes, Seconds and Milli Seconds
     */
    public String diffTime(){
        long diff = diff();
        long min = TimeUnit.NANOSECONDS.toMinutes(diff);
        long sec = TimeUnit.NANOSECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(min);
        long ms =  TimeUnit.NANOSECONDS.toMillis(diff) - (TimeUnit.MINUTES.toMillis(min) +  TimeUnit.SECONDS.toMillis(sec));
        
        String time = String.format("%02d min %02d sec %d ms",
                min, sec, ms
        );
        
        try {
            AuditLog.results(time, false);
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return String.format("Duration: \u001B[32m%s\u001B[0m", time);
    }
}
