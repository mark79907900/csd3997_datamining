/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datamining.Utils;

import com.sun.javafx.fxml.PropertyNotFoundException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;

/**
 * This class is responsible with the config file
 * @author Mark
 */
public class Config {
    private Properties properties;
    
    /**
     * Get the Config from the passed destination and load them in the properties object
     * @param configDestination
     */
    public Config(String configDestination){
        properties = new Properties();
        
        try(InputStream inputStream = new FileInputStream(configDestination)){
            properties.load(inputStream);
            inputStream.close();
        }catch(FileNotFoundException ex){
            ErrorHandling.output(Level.CONFIG, ex);
        }catch(Exception ex){
            ErrorHandling.output(Level.CONFIG, ex);
        }
    }
    
    /**
     * Method used to get the value for the passed property name
     * @param property
     * @return
     */
    public String getPropertyString(String property){
        String value = "";
        try{
            value = properties.getProperty(property);
        }catch(PropertyNotFoundException e){
            ErrorHandling.output(Level.CONFIG, e);
        }
        return value;
    }
    
    /**
     * Method used to get the value for the passed property name
     * @param property
     * @return
     */
    public String getPropertyHTML(String property){
        String value = "";
        try{
            value = String.format(
                    "<html> <p width=\"250\">%s</p></html>", 
                    properties.getProperty(property)
            );
        }catch(PropertyNotFoundException e){
            ErrorHandling.output(Level.CONFIG, e);
        }
        return value;
    }
    
    /**
     * Method used to get the INT value for the passed property name
     * @param property
     * @return
     */
    public int getPropertyInt(String property){
        int value = 0;
        try{
            value = Integer.parseInt(properties.getProperty(property));
        }catch(PropertyNotFoundException | NullPointerException e){
            ErrorHandling.output(Level.CONFIG, e);
        }catch(Exception e){
            ErrorHandling.output(Level.CONFIG, e);
        }
        return value;
    }
    
    /**
     * Method used to get the INT value for the passed property name
     * @param property
     * @return
     */
    public long getPropertyLong(String property){
        long value = 0;
        try{
            value = Long.parseLong(properties.getProperty(property));
        }catch(PropertyNotFoundException | NullPointerException e){
            ErrorHandling.output(Level.CONFIG, e);
        }catch(Exception e){
            ErrorHandling.output(Level.CONFIG, e);
        }
        return value;
    }
    
    /**
     * Method used to get the Boolean value for the passed property name
     * @param property
     * @return 
     */
    public boolean getPropertyBoolean(String property){
        boolean value = false;
        try{
            value = Boolean.parseBoolean(properties.getProperty(property));
        }catch(PropertyNotFoundException | NullPointerException e){
            ErrorHandling.output(Level.CONFIG, e);
        }catch(Exception e){
            ErrorHandling.output(Level.CONFIG, e);
        }
        return value;
    }
    
}
