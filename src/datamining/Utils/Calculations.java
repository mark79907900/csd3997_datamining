/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining.Utils;

import datamining.Types.GPSRow;
import geodesy.Ellipsoid;
import geodesy.GeodeticCalculator;
import geodesy.GlobalPosition;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Calculations {
    
    /**
     * Calculate the distance between two WGS84 Coordinates
     * The calculations are based on Vincenty's formulae 
     * @param latA
     * @param lonA
     * @param latB
     * @param lonB
     * @return
     */
    public static double distanceBetweenGPSPositions
        (double latA, double lonA, double latB, double lonB){
            
        GeodeticCalculator geoCalc = new GeodeticCalculator();
        
        Ellipsoid reference = Ellipsoid.WGS84;
        GlobalPosition pointA = new GlobalPosition(latA, lonA, 0.0); // Point A
        GlobalPosition pointB = new GlobalPosition(latB, lonB, 0.0); // Point B
        
        // Distance between Point A and Point B
        return geoCalc.calculateGeodeticCurve
        (reference, pointB, pointA).getEllipsoidalDistance(); 
    }
    
    /**
     * Calculate the euclidean distance Distance between two gps rows.
     * The sqr root operation is omitted to enhance performance 
     * @param testRow
     * @param trainingRow
     * @return
     */
    public static double euclideanDistance(GPSRow testRow, GPSRow trainingRow){
        double returnValue = 0;
        try{
            returnValue = Math.pow(trainingRow.getLat() - testRow.getLat(), 2) + Math.pow(trainingRow.getLon()- testRow.getLon(), 2);
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return returnValue;
    }
}
